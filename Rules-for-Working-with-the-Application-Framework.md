# Rules for Working with the Project's Application Framework
In order to have your code approved for merging into the Northwind Traders Project, you need to make sure that the 
code comports well with our Application Framework. Therefore, we have placed this file on the root of the repo so 
you will be able to check out the appropriate documentation to learn how to set up your module projects and the 
files to work well with the Application Framework. We want everyone's contributions to be allowed, so we want to 
help you out as much as possible. See [The Application Framework](https://gitlab.com/gs-unitedlabs/northwind-traders/-/wikis/developer-section/The-Application-Framework) for information about the GS United Labs Application Framework.

### Application Framework Example Project
To get some hands-on experience working with the Application Framework, check out our article on creating an application using the Application 
Framework: [Basic Application Example](https://gitlab.com/gs-unitedlabs/northwind-traders/-/wikis/developer-section/Basic-Application-Example).
We will be updating this article with more Framework feature examples in the future...

## The Project Structure
For your project to be accepted into the code-base, you need to set it up according to these rules:

1. You need to define specific packages.
   * The package where your classes are located, for example: `com.myco.desktop`
   * The package where your resources are located. Must be a sub-package of the class(es) for which the resources 
are used, for example: `com.myco.desktop.resources`
2. The property file for the class ***must have*** the same name as the class. For example, if you have a class 
called `MyFrame.java` located in the package `com.myco.desktop`, then your properties file must be:
   * Named: `MyFrame.properties`
   * Located: `com.myco.desktop`

As long as these simple rules are followed, your project structure will be considered correct. For more information, 
see [Designing Windows](https://gitlab.com/gs-unitedlabs/northwind-traders/-/wikis/developer-section/Using-the-Application-Framework#Designing-Windows).

## Modules
To properly set up your `module-info.java` file(s), see [Setting up the module-info.java Properly](https://gitlab.com/gs-unitedlabs/northwind-traders/-/wikis/developer-section/Using-the-Application-Framework#setting-up-the-module-infojava-properly).

## Form Editor in NetBeans
To properly set up your NetBeans IDE Matisse GUI Builder's Form Editor, see [Setting up the Form Editor](https://gitlab.com/gs-unitedlabs/northwind-traders/-/wikis/developer-section/Using-the-Application-Framework#setting-up-the-form-editor).

## Resource Injection Setup
To properly prepare your project for using the Application Framework's resource injection feature, see [That takes Care of Properties, but What About Resources?](https://gitlab.com/gs-unitedlabs/northwind-traders/-/wikis/developer-section/Using-the-Application-Framework#that-takes-care-of-properties-but-what-about-resources).

## Using Actions, Specifically `@Action`s
To properly use the `@Action` annotation provided by the Application Framework, see [Handling Properties and Resources for Actions](https://gitlab.com/gs-unitedlabs/northwind-traders/-/wikis/developer-section/Using-the-Application-Framework#handling-properties-and-resources-for-actions).

## Customizing Form Editor-Created Code
To be able to setup the project to properly utilize all of the features of the Application Framework, you will need to be able to edit the 
"uneditable" code block, `initComponents`. NetBeans provides the ability to do this, see [OK, so How do I Add the Action to the Button?](https://gitlab.com/gs-unitedlabs/northwind-traders/-/wikis/developer-section/Using-the-Application-Framework#ok-so-how-do-i-add-the-action-to-the-button).

# Conclusion
If you follow all of the rules and provide quality, clean code, there is no reason that your code cannot (and *should not*) be included in the 
Northwind Traders Project's code-base. We look forward to your participation in the Project!
