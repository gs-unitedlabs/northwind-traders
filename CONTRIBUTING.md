# Contributing to Northwind Traders
:+1: :tada: First of all...**Thank You** for taking the time to contribute! :tada: :+1:

The following is a set of guidelines for contributing to Northwind Traders, which is hosted in the GS United Labs Group on GitLab. These are mostly guidelines, not rules. Use your best judgment, and feel free to propose changes to this document in a pull request.

## Code of Conduct
This project and everyone participating in it is governed by the [NTA Code of Conduct](https://gitlab.com/gs-unitedlabs/northwind-traders/-/blob/main/CONDUCT.md). By participating, you are expected to uphold this code. Please report unacceptable behavior to [Sean Carrick, Project Organizer and Lead Developer](sean@gs-unitedlabs.com).

## I do not want to read this whole thing! I just have a question...

> ***Note***: Please, do not fill out an issue to ask a question. Just refer to this document to see if your question is answered.

If you still have questions after reading this document, then goto our [Wiki](https://gitlab.com/gs-unitedlabs/northwind-traders/-/wikis/home) because your question may be answered there.

## What Should I know before I get Started?
The Northwind Traders Basic Edition Project is being designed to fill a niche for accounting packages in an industry that has vastly different needs for financial breakdown of information – The trucking industry.
The trucking industry is a per mile industry, where the only financial information that matters is based upon the number of miles a unit may travel. These miles are either logged as empty or loaded, depending upon whether the unit is hauling a load from Point A to Point B, or deadheading from Point B to Point C to pick up another load. Regardless of whether the truck is loaded or empty, there are multiple costs involved in the unit movement, such as fuel, maintenance, tolls/parking, etc. Even when the unit is not actively hauling a load, if it is moving, it is incurring these costs. This is why the financial breakdown that makes the most sense to the trucking industry is the per mile breakdown. While empty, units typically get higher fuel economy and lower maintenance costs than when loaded. However, every mile moved costs the company money. All of the expenses that I’ve mentioned so far don’t even take into account the pay for the driver of the unit, who is usually also paid per mile when the unit is empty, as well as when the unit is loaded.
Northwind Traders Basic Edition Project aims to answer this lack of software that tracks revenue and expenses per mile.

### The Development System
Northwind Traders Basic Edition Project is to be developed in the Java™ Programming Language, as it is system independent. By using Java, we will be able to design and code the project once and it will be able to be used on any operating system that supports a Java Virtual Machine.

Furthermore, the Project will be designed in a modular fashion. In this way, end users will only need to purchase those portions of the application that make sense for their business setup and structure. For this reason, not only are we developing the Project in the Java™ Programming Language, but we are using Java 11, which is modular by design.

Since our Project is to have a graphical user interface (GUI), and we do not want to spend a decade or more creating the application plumbing, the Project is going to be built upon the JSR-296 Swing Application Framework. This Framework establishes all of the application plumbing, from launch to initialization to startup to shutdown, and makes sure that the application is run on the Event Dispatching Thread (EDT). Furthermore, this Framework enables us to use Actions to handle all menu and toolbar commands and only need to write those functions a single time, which will make maintenance of the code simpler moving forward. Another benefit of using this Framework is that we will be able to move all process-intensive tasks onto a background thread, thereby decreasing the likelihood of our application being “locked up” due to these processes.

Also, we are building our Project as a *platform*, meaning that all windows and dialogs are dynamically loaded, as well as the menu items and toolbar buttons. We have created an API that makes providing the dynamic parts of the application easier for developers. Also, because we are developing the application in a modular fashion, modules may provide about panels for the module, as well as add panels to the application's Options dialog via the platform API.

#### Proper Git Procedures
Participation in the Northwind Traders Basic Edition Project requires that all proper procedures are always followed, especially these *Proper Git Procedures*. The procedures are:

1. `git reset --hard HEAD`: This gets your local repository completely up-to-date with the GitLab repository.
2. `git checkout main`: This puts your local repository on the `main` branch of the repo.
3. `git pull`: This brings down all of the most recent changes from the GitLab repository.
4. ***make changes***: This is when you make any changes to the code in your local repository.
5. `git checkout -b <new branch name>`: Create a new branch to which your changes can be added. The `<new branch name>` needs to start with your 
user name (i.e., scarrick), followed by a slash (/), and finally a brief title for the changes made.
6. `git add .`: This adds all changed files to your newly created branch. **Important** &mdash; *Do not* forget to add the dot at the end of this 
command.
7. `git commit`: This will commit your changes to your local repository and pop open your default terminal text editor into which you ***must*** enter a commit message. This message needs to specify the changes you made, the reason for the changes, what bugs/issues were fixed (if any), what RFEs were added (if any), etc. This message has no length limit on it, so be detailed and use markdown within your message. Also, you may enter a milestone to which the commit belongs by using the marker (`/milestone %"milestone"`), where "milestone" is the name of the milestone. You may enter one or more labels by using the marker (`label ~label1 ~"label2"`), where "label1" and "label2" are the names of labels. Furthermore, we appreciate knowing how much time was spent on various tasks. If you are working on fixing an issue and that issue has a time estimate, place time tracking markers into your commit message by using the markers (`/estimated 1d 5h 30m` and `/spent 1d 2h 4m`). Time tracking may be entered as positive or negative amounts of time, where positive amounts decrease the time remaining on an estimate and negative amounts of time remove time spent.
8. `git push`: This actually pushes your changes to the remote GitLab repository.

Once you have completed all of these steps, you must then go to the GitLab Merge requests tab and create your merge request. Once your MR is created, one of the primary members of the repository will review your MR to verify that it meets the Project Standards (which are discussed in this document).

This seems like it's a lot of work, doesn't it? Well, we thought so too, which is why we have placed two Bash Shell Scripts on the root of the repository to automate a lot of these steps. For steps 1-3, you simply run the following command in a terminal window, in the root folder of your local repository: `./git-reset.sh HEAD`. After this script runs (it will prompt you for logging into GitLab), you will then be able to perform Step 4: make changes.

Once you have made the changes you want, drop back to a terminal window in the root of your repository and run `./git-push.sh userName/ChangeTitle`. This will automate Steps 5-8 for you, popping up your default terminal editor to enter your commit message and prompting you for your GitLab credentials, as needed.

We are hoping to have Windows batch file equivalent scripts before too much longer...

### The Java JDK11
By building the Project in JDK11, we will be able to take advantage of the long term support for this version of the JDK, as well as taking advantage of the Jigsaw Project that was introduced in JDK9, which allows for the modular development paradigm.

Do not worry about your skill level in Java. We accept contributors of all skill levels, as all of us had to start somewhere to become the programmers that we currently are. If you do not have a lot of experience in developing modular Java projects, we will help you as much as you need so that you can learn and grow as a developer.

#### API and Overall Design Decisions
Whenever you have a question about the API or how the project is designed, we refer you to our [Wiki Pages](https://gitlab.com/gs-unitedlabs/northwind-traders/-/wikis/home). The Wiki Pages' landing page is simply a directory for you to find what you are looking for. The landing page is simply a clickable table of contents that will take you to the top page of each of our sections. If you are a contributor doing documentation, you will want to click the link for documentation. Likewise, if you are contributing code to the Project, you will want to click on the developers' section to access design charts and API documentation.

> ***DEC 03, 2021***: The Wiki has been started and there is now content in the Developers Section. Make sure to check out the information about the 
Application Framework.
> ***NOV 06, 2021***: As of this date, our Wiki has not been started yet, but it will be coming soon, so make sure to check back.

## How Can I contribute
### Reporting Bugs
When running the project either in an Integrated Development Environment (IDE) or outside of one, if an error occurs, typically, a Report Bug dialog will pop up. When this happens, submit the bug report to us. This takes place via email and automatically attaches the application's log files and any error log files that may exist. Also, the submission of the bug report automatically creates an issue on the repository. Since this is done by email message, the reporter will be notified of any updates to the issue they submitted.

### Suggesting Enhancements
Prior to suggesting an enhancement, we request that you search our issues database for all submitted enhancements (this is a label on the issues). If your enhancement has already been submitted, then we ask that you not submit it again. While reviewing the submitted enhancements, if you find one similar to yours, check its status to see if it has yet been created. If it has not, then upvote the Request for Enhancement (RFE). We tend to work on those enhancements that more people are interested in first.

## Style Guide
When commiting contributions to the repository, you can always try to make commit messages more appealing by using appropriate emojis, keep things in the present, keep things active, and keep the first line (which becomes the commit title) as short as possible. Here are some examples of these items:

- Use the present tense ("Add feature", instead of "Added feature")
- Use the imperitive modde ("Move cursor to..." instead of "Moves cursor to...")
- Limit the first line to no more than 65 characters
- Reference Issues and Pull Requests (PRs) ***after*** the first line
- When only changing documentation, include `[ci skip]` in the commit title (first line of commit message)
- Consider starting the commit message with an emoji:
   - :art: `:art:` when improving the format/structure of code
   - :racehorse: `:racehorse:` when improving performance
   - :non-potable_water: `:non-potable_water:` when plugging memory leaks
   - :memo: `:memo:` when writing documentation
   - :penguin: `:penguin:` when the fix is for Linux
   - :apple: `:apple:` when the fix is for Mac OS
   - :checkered_flag: `:checkered_flag:` when the fix is for Windows
   - :bug: `:bug:` when fixing a bug
   - :fire: `:fire:` when removing code or files
   - :green_heart: `:green_heart:` when fixing teh CI build
   - :white_check_mark: `:white_check_mark:` when adding tests
   - :lock: `:lock:` when dealing with security
   - :arrow_up: `:arrow_up:` when upgrading dependencies
   - :arrow_down: `:arrow_down:` when downgrading dependencies
   - :shirt: `:shirt:` when removing warnings

## Java Style Guide
When it comes to the actual coding style for the Project, we ask that you respect this style guide when writing your code submissions.

- Do your best to keep all lines within the code file to 80 characters in length or less
   - Use proper line breaking to follow this rule
      - Break on the dot operator
      - Keep symbols (+, ?, :, &&, ||, etc.) with the code that *follows* the symbol
      - Break strings on a space so the next line does not start with a space
- Always indent blocks by 4 spaces
- Always use the curly braces ({}) around `if`, `else if`, and `else` blocks, even if the entire block is only one line of code
- Use the `Logger` functionality whenever possible
- Use comments sparingly...this is what the `Logger` is for
   - When using single-line comments, if the comment goes beyond the 80 character mark, make the next line start with `//+ ` to indicate that the comment is continuing
- Always place JavaDoc comments on public fields and methods
   - You may use markdown in the JavaDoc comments
   - Always tell what the method throws, even unchecked exceptions such as `IllegalArgumentException`

### JavaDoc Example
```java
/** 
 * The `ExampleClass` provides the features described in this JavaDoc
 * comment. We will even give you some examples of *how* to use the
 * class.
 *
 * Each paragraph only needs to be separated by a blank line. Since
 * we use `doxygen` for generating the JavaDocs, the markdown works
 * just fine.
 *
 * @see #exampleMethod(java.lang.String)
 *
 * We still use some JavaDoc annotations just to make things a bit
 * easier.
 */
 ```
