# Northwind Trader Basic Edition
![NTA Logo](src/NTA.Foundation/classes/com/gs/nta/view/resources/icons/NorthwindSplashScreen.png)

## Overview
The Northwind Traders Basic Edition Project is being designed to fill a niche for accounting packages in an industry that has vastly different needs for financial breakdown of information &mdash; The trucking industry.

The trucking industry is a per mile industry, where the only financial information that matters is based upon the number of miles a unit may travel. These miles are either logged as empty or loaded, depending upon whether the unit is hauling a load from Point A to Point B, or deadheading from Point B to Point C to pick up another load. Regardless of whether the truck is loaded or empty, there are multiple costs involved in the unit movement, such as fuel, maintenance, tolls/parking, etc. Even when the unit is not actively hauling a load, if it is moving, it is incurring these costs. This is why the financial breakdown that makes the most sense to the trucking industry is the per mile breakdown. While empty, units typically get higher fuel economy and lower maintenance costs than when loaded. However, every mile moved costs the company money. All of the expenses that I’ve mentioned so far don’t even take into account the pay for the driver of the unit, who is usually also paid per mile when the unit is empty, as well as when the unit is loaded.

Northwind Traders Basic Edition Project aims to answer this lack of software that tracks revenue and expenses per mile.

## The Development System
Northwind Traders Basic Edition Project is to be developed in the Java&trade; Programming Language, as it is system independent. By using Java, we will be able to design and code the project once and it will be able to be used on any operating system that supports a Java Virtual Machine.

Furthermore, the Project will be designed in a modular fashion. In this way, end users will only need to purchase those portions of the application that make sense for their business setup and structure. For this reason, not only are we developing the Project in the Java&trade; Programming Language, but we are using Java 11, which is modular by design.

Since our Project is to have a graphical user interface (GUI), and we do not want to spend a decade or more creating the application plumbing, the Project is going to be built upon the GS United Labs Platform Application Framework. This Framework establishes all of the application plumbing, from launch to initialization to startup to shutdown, and makes sure that the application is run on the Event Dispatching Thread (EDT). Furthermore, this Framework enables us to use Actions to handle all menu and toolbar commands and only need to write those functions a single time, which will make maintenance of the code simpler moving forward. Another benefit of using this Framework is that we will be able to move all process-intensive tasks onto a background thread, thereby decreasing the likelihood of our application being “locked up” due to these processes.
