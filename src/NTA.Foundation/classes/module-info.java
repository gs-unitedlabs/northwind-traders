/*
 * Copyright (C) 2022 PekinSOFT Systems
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * *****************************************************************************
 * *****************************************************************************
 *  Project    :   NTA-Basic
 *  Class      :   module-info.java
 *  Author     :   Sean Carrick
 *  Created    :   Jan 25, 2022 @ 12:47:48 PM
 *  Modified   :   Jan 25, 2022
 *  
 *  Purpose:
 *  
 *  Revision History:
 *  
 *  WHEN          BY                  REASON
 *  ----------    ------------------- ------------------------------------------
 *  Jan 25, 2022       Sean Carrick             Initial creation.
 * *****************************************************************************
 */

open module NTA.Foundation {
    requires gs.platform.api;
    requires java.desktop;
    requires java.logging;
    requires org.apache.derby.engine;
    requires org.apache.derby.client;
    requires org.apache.derby.server;
    requires org.apache.derby.optionaltools;
    requires org.apache.derby.runner;
    requires org.apache.derby.commons;
    requires org.apache.derby.tools;
    requires JTattoo;
    requires swingx;
    
    exports com.gs.nta;
    exports com.gs.nta.view;
    
    uses com.gs.spi.bugs.BugReporter;
    uses com.gs.spi.modules.Module;
    
    provides com.gs.spi.modules.Module with
            com.gs.nta.FoundationModule;
}
