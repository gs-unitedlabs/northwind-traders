/*
 * Copyright (C) 2022 PekinSOFT Systems
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * *****************************************************************************
 *  Project    :   NTA-Basic
 *  Class      :   FileMenu.java
 *  Author     :   Sean Carrick
 *  Created    :   Jan 25, 2022 @ 5:48:11 PM
 *  Modified   :   Jan 25, 2022
 *  
 *  Purpose: See class JavaDoc comments.
 *  
 *  Revision History:
 *  
 *  WHEN          BY                  REASON
 *  ------------  ------------------- ------------------------------------------
 *  Jan 25, 2022  Sean Carrick        Initial creation.
 * *****************************************************************************
 */

package com.gs.nta.view;

import com.gs.spi.desktop.MenuProvider;

/**
 *
 * @author Sean Carrick &lt;sean at pekinsoft dot com&gt;
 * 
 * @version 0.1.0
 * @since 0.1.0
 */
public class FileMenu implements MenuProvider {
    
    public FileMenu () {
        
    }

    @Override
    public String getName() {
        // TODO: Provide implementation of FileMenu.getName
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public int getPosition() {
        // TODO: Provide implementation of FileMenu.getPosition
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public int compareTo(MenuProvider o) {
        // TODO: Provide implementation of FileMenu.compareTo
        throw new UnsupportedOperationException("Not supported yet.");
    }

}
