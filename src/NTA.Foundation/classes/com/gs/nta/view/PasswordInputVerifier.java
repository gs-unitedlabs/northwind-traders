/*
 * Copyright (C) 2022 PekinSOFT Systems
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * *****************************************************************************
 * *****************************************************************************
 *  Project    :   NTA-Basic
 *  Class      :   PasswordInputVerifier.java
 *  Author     :   Sean Carrick
 *  Created    :   Jan 25, 2022 @ 10:51:48 AM
 *  Modified   :   Jan 25, 2022
 *  
 *  Purpose:
 *  
 *  Revision History:
 *  
 *  WHEN          BY                  REASON
 *  ----------    ------------------- ------------------------------------------
 *  Jan 25, 2022       Sean Carrick             Initial creation.
 * *****************************************************************************
 */

package com.gs.nta.view;

import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.swing.InputVerifier;
import javax.swing.JComponent;
import javax.swing.JPasswordField;

/**
 *
 * @author Sean Carrick &lt;sean at pekinsoft dot com&gt;
 * 
 * @version 0.1.0
 * @since 0.1.0
 */
public class PasswordInputVerifier extends InputVerifier {

    public PasswordInputVerifier () {
        
    }

    @Override
    public boolean verify(JComponent input) {
        String regex = "^(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])(?=.*[!_@#$%^&-+=()])(?=\\S+$).{8,20}$";
        Pattern pattern = Pattern.compile(regex);
        String pword = String.copyValueOf(((JPasswordField) input).getPassword());
        
        Matcher matcher = pattern.matcher(pword);
        
        return matcher.matches();
    }

}
