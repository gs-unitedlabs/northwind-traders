/*
 * Copyright (C) 2022 PekinSOFT Systems
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * *****************************************************************************
 *  Project    :   NTA-Basic
 *  Class      :   OptionsDialog.java
 *  Author     :   Sean Carrick
 *  Created    :   Jan 26, 2022 @ 7:11:45 AM
 *  Modified   :   Jan 26, 2022
 *  
 *  Purpose: See class JavaDoc comments.
 *  
 *  Revision History:
 *  
 *  WHEN          BY                  REASON
 *  ------------  ------------------- ------------------------------------------
 *  Jan 26, 2022  Sean Carrick        Initial creation.
 * *****************************************************************************
 */
package com.gs.nta.view;

import com.gs.application.Action;
import com.gs.application.ApplicationContext;
import com.gs.application.ResourceMap;
import com.gs.application.SingleFrameApplication;
import com.gs.nta.NTApp;
import com.gs.spi.modules.Module;
import com.gs.spi.desktop.OptionsPanelProvider;
import com.gs.spi.logging.Level;
import com.gs.utils.MessageBox;
import java.awt.CardLayout;
import java.awt.Component;
import java.awt.Desktop;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import javax.swing.ActionMap;
import javax.swing.JTabbedPane;

/**
 *
 * @author Sean Carrick
 */
public class OptionsDialog extends javax.swing.JDialog {

    private final ApplicationContext context;
    private final ResourceMap resMap;
    private final ActionMap actMap;
    private final List<OptionsPanelProvider> panels;
    
    private boolean saveNeeded;

    /**
     * Creates new form OptionsDialog
     *
     * @param context the context of the `Application` this dialog is for
     * @param modal whether or not this dialog should block
     */
    public OptionsDialog(ApplicationContext context, boolean modal) {
        super(((SingleFrameApplication) context.getApplication()).getMainFrame(), modal);

        this.context = context;
        resMap = context.getResourceMap(getClass());
        actMap = context.getActionMap(this);

        panels = new ArrayList<>();

        initComponents();
    }
    
    public boolean isSaveNeeded() {
        return saveNeeded;
    }
    
    public void setSaveNeeded(boolean saveNeeded) {
        boolean oldValue = isSaveNeeded();
        this.saveNeeded = saveNeeded;
        
        firePropertyChange("saveNeeded", oldValue, isSaveNeeded());
    }

    @Action(enabledProperty = "saveNeeded")
    public void apply() {
        setSaveNeeded(false);

        panels.forEach(p -> {
            p.saveSettings(((NTApp) context.getApplication())
                    .getProperties());
        });
    }

    @Action(enabledProperty = "saveNeeded")
    public void applyAndClose() {
        apply();
        cancel();
    }

    @Action
    public void cancel() {
        ((NTApp) context.getApplication()).hide(this);
    }

    @Action
    public void showHelp() {
        URL helpUrl = null;
        String msg = "The current options panel has no\n"
                + "help associated with it.\n\n"
                + "Displaying Options dialog help instead.";
        String ttl = "Help Not Found";

        for (Component c : getComponents()) {
            if (c instanceof JTabbedPane) {
                JTabbedPane pane = (JTabbedPane) c;

                for (int x = 0; x < pane.getTabCount(); x++) {
                    for (OptionsPanelProvider p : panels) {
                        if (p.getInstance().equals(pane.getTabComponentAt(x))) {
                            try {
                                helpUrl = p.getHelpURL();
                            } catch (MalformedURLException ex) {
                                context
                                        .getApplication()
                                        .getLogger()
                                        .log(Level.WARN, String.format("Invalid "
                                                + "help URL for panel %s",
                                                p.getInstance()));
                            }
                            break;
                        }
                    }
                }
            }
        }

        if (helpUrl != null) {
            try {
                Desktop.getDesktop().browse(helpUrl.toURI());
            } catch (IOException | URISyntaxException ex) {
                context
                        .getApplication()
                        .getLogger()
                        .log(Level.WARN, String.format("URI Sytax is invalid "
                                + "for panel: %s", ex));

                int choice = MessageBox.askQuestion(msg, ttl, false);
                if (choice == MessageBox.YES_OPTION) {
                    try {
                        helpUrl = new URL("https://gitlab.com/gs-unitedlabs/northwind"
                                + "-traders/-/wikis/user-section/Options-Dialog");
                        Desktop.getDesktop().browse(helpUrl.toURI());
                    } catch (IOException | URISyntaxException e) {
                        context
                                .getApplication()
                                .getLogger()
                                .log(Level.WARN, "Unable to browse to the "
                                        + "default help page.");
                    }
                }
            }
        }
    }

    @Action
    public void general() {
        ((CardLayout) getLayout()).show(categoriesPanel, "general");
    }

    @Action
    public void accounting() {
        ((CardLayout) getLayout()).show(categoriesPanel, "accounting");
    }

    @Action
    public void actions() {
        ((CardLayout) getLayout()).show(categoriesPanel, "actions");
    }

    @Action
    public void reporting() {
        ((CardLayout) getLayout()).show(categoriesPanel, "reporting");
    }

    @Action
    public void lookAndFeel() {
        ((CardLayout) getLayout()).show(categoriesPanel, "interface");
    }

    @Action
    public void misc() {
        ((CardLayout) getLayout()).show(categoriesPanel, "misc");
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        categoriesButtonGroup = new javax.swing.ButtonGroup();
        categoriesToolbar = new javax.swing.JToolBar();
        generalButton = new javax.swing.JToggleButton();
        accountingButton = new javax.swing.JToggleButton();
        actionsButton = new javax.swing.JToggleButton();
        reportsButton = new javax.swing.JToggleButton();
        interfaceButton = new javax.swing.JToggleButton();
        miscButton = new javax.swing.JToggleButton();
        commandPanel = new javax.swing.JPanel();
        jSeparator1 = new javax.swing.JSeparator();
        buttonPanel = new javax.swing.JPanel();
        cancelButton = new javax.swing.JButton();
        applyButton = new javax.swing.JButton();
        okButton = new javax.swing.JButton();
        helpButton = new javax.swing.JButton();
        categoriesPanel = new javax.swing.JPanel();
        generalTabs = new javax.swing.JTabbedPane();
        accountingTabs = new javax.swing.JTabbedPane();
        actionsTabs = new javax.swing.JTabbedPane();
        reportingTabs = new javax.swing.JTabbedPane();
        interfaceTabs = new javax.swing.JTabbedPane();
        miscTabs = new javax.swing.JTabbedPane();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setName("Form"); // NOI18N

        categoriesToolbar.setBackground(new java.awt.Color(26, 26, 26));
        categoriesToolbar.setForeground(new java.awt.Color(242, 242, 242));
        categoriesToolbar.setRollover(true);
        categoriesToolbar.setName("categoriesToolbar"); // NOI18N

        generalButton.setBackground(new java.awt.Color(26, 26, 26));
        categoriesButtonGroup.add(generalButton);
        generalButton.setForeground(new java.awt.Color(242, 242, 242));
        generalButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/gs/nta/view/resources/icons/OptionsToolbar/applications-internet.png"))); // NOI18N
        generalButton.setSelected(true);
        generalButton.setText("General");
        generalButton.setFocusable(false);
        generalButton.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        generalButton.setName("generalButton"); // NOI18N
        generalButton.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        categoriesToolbar.add(generalButton);

        accountingButton.setBackground(new java.awt.Color(26, 26, 26));
        categoriesButtonGroup.add(accountingButton);
        accountingButton.setForeground(new java.awt.Color(242, 242, 242));
        accountingButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/gs/nta/view/resources/icons/OptionsToolbar/applications-mathematics.png"))); // NOI18N
        accountingButton.setText("Accounting");
        accountingButton.setFocusable(false);
        accountingButton.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        accountingButton.setName("accountingButton"); // NOI18N
        accountingButton.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        categoriesToolbar.add(accountingButton);

        actionsButton.setBackground(new java.awt.Color(26, 26, 26));
        categoriesButtonGroup.add(actionsButton);
        actionsButton.setForeground(new java.awt.Color(242, 242, 242));
        actionsButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/gs/nta/view/resources/icons/OptionsToolbar/preferences-calendar-and-tasks.png"))); // NOI18N
        actionsButton.setText("Action Items");
        actionsButton.setFocusable(false);
        actionsButton.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        actionsButton.setName("actionsButton"); // NOI18N
        actionsButton.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        categoriesToolbar.add(actionsButton);

        reportsButton.setBackground(new java.awt.Color(26, 26, 26));
        categoriesButtonGroup.add(reportsButton);
        reportsButton.setForeground(new java.awt.Color(242, 242, 242));
        reportsButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/gs/nta/view/resources/icons/OptionsToolbar/applications-publishing.png"))); // NOI18N
        reportsButton.setText("Reporting");
        reportsButton.setFocusable(false);
        reportsButton.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        reportsButton.setName("reportsButton"); // NOI18N
        reportsButton.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        categoriesToolbar.add(reportsButton);

        interfaceButton.setBackground(new java.awt.Color(26, 26, 26));
        categoriesButtonGroup.add(interfaceButton);
        interfaceButton.setForeground(new java.awt.Color(242, 242, 242));
        interfaceButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/gs/nta/view/resources/icons/OptionsToolbar/applications-interfacedesign.png"))); // NOI18N
        interfaceButton.setText("Interface");
        interfaceButton.setFocusable(false);
        interfaceButton.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        interfaceButton.setName("interfaceButton"); // NOI18N
        interfaceButton.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        categoriesToolbar.add(interfaceButton);

        miscButton.setBackground(new java.awt.Color(26, 26, 26));
        categoriesButtonGroup.add(miscButton);
        miscButton.setForeground(new java.awt.Color(242, 242, 242));
        miscButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/gs/nta/view/resources/icons/OptionsToolbar/applications-accessories.png"))); // NOI18N
        miscButton.setText("Miscellaneous");
        miscButton.setFocusable(false);
        miscButton.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        miscButton.setName("miscButton"); // NOI18N
        miscButton.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        categoriesToolbar.add(miscButton);

        getContentPane().add(categoriesToolbar, java.awt.BorderLayout.PAGE_START);

        commandPanel.setName("commandPanel"); // NOI18N
        commandPanel.setLayout(new java.awt.BorderLayout());

        jSeparator1.setName("jSeparator1"); // NOI18N
        commandPanel.add(jSeparator1, java.awt.BorderLayout.PAGE_START);

        buttonPanel.setName("buttonPanel"); // NOI18N

        cancelButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/gs/nta/view/resources/icons/small/close-alt.png"))); // NOI18N
        cancelButton.setText("Cancel");
        cancelButton.setName("cancelButton"); // NOI18N
        cancelButton.setAction(actMap.get("cancel"));

        applyButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/gs/nta/view/resources/icons/small/apply.png"))); // NOI18N
        applyButton.setText("Apply");
        applyButton.setName("applyButton"); // NOI18N
        applyButton.setAction(actMap.get("apply"));

        okButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/gs/nta/view/resources/icons/small/accept.png"))); // NOI18N
        okButton.setText("OK");
        okButton.setName("okButton"); // NOI18N
        okButton.setAction(actMap.get("applyAndClose"));

        helpButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/gs/nta/view/resources/icons/small/help-contents.png"))); // NOI18N
        helpButton.setText("help");
        helpButton.setName("helpButton"); // NOI18N
        helpButton.setAction(actMap.get("showHelp"));

        javax.swing.GroupLayout buttonPanelLayout = new javax.swing.GroupLayout(buttonPanel);
        buttonPanel.setLayout(buttonPanelLayout);
        buttonPanelLayout.setHorizontalGroup(
            buttonPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, buttonPanelLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(helpButton)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 646, Short.MAX_VALUE)
                .addComponent(okButton)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(applyButton)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(cancelButton)
                .addContainerGap())
        );
        buttonPanelLayout.setVerticalGroup(
            buttonPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(buttonPanelLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(buttonPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(cancelButton)
                    .addComponent(applyButton)
                    .addComponent(okButton)
                    .addComponent(helpButton))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        commandPanel.add(buttonPanel, java.awt.BorderLayout.CENTER);

        getContentPane().add(commandPanel, java.awt.BorderLayout.PAGE_END);

        categoriesPanel.setName("categoriesPanel"); // NOI18N
        categoriesPanel.setLayout(new java.awt.CardLayout());

        generalTabs.setName("generalTabs"); // NOI18N
        categoriesPanel.add(generalTabs, "general");

        accountingTabs.setName("accountingTabs"); // NOI18N
        categoriesPanel.add(accountingTabs, "accounting");

        actionsTabs.setName("actionsTabs"); // NOI18N
        categoriesPanel.add(actionsTabs, "actions");

        reportingTabs.setName("reportingTabs"); // NOI18N
        categoriesPanel.add(reportingTabs, "reporting");

        interfaceTabs.setName("interfaceTabs"); // NOI18N
        categoriesPanel.add(interfaceTabs, "interface");

        miscTabs.setName("miscTabs"); // NOI18N
        categoriesPanel.add(miscTabs, "misc");

        getContentPane().add(categoriesPanel, java.awt.BorderLayout.CENTER);
        setTitle(resMap.getString("Form.title"));

        for (Module m : ((NTApp) context.getApplication()).getInstalledModules()) {
            panels.addAll(m.getOptionsPanel());
        }

        Collections.sort(panels);

        panels.forEach(p -> {

            p.loadSettings(((NTApp) context.getApplication()).getProperties());

            switch (p.getCategory()) {
                case GENERAL:
                generalTabs.addTab(p.getTitle(), p.getInstance());
                break;
                case ACCOUNTING:
                accountingTabs.addTab(p.getTitle(), p.getInstance());
                break;
                case INTERFACE:
                interfaceTabs.addTab(p.getTitle(), p.getInstance());
                break;
                case MISCELLANEOUS:
                miscTabs.addTab(p.getTitle(), p.getInstance());
                break;
                case REPORTS:
                reportingTabs.addTab(p.getTitle(), p.getInstance());
                break;
                case SCHEDULING:
                actionsTabs.addTab(p.getTitle(), p.getInstance());
                break;
                default:
                generalTabs.addTab(p.getTitle(), p.getInstance());
                break;
            }
        });

        pack();
    }// </editor-fold>//GEN-END:initComponents


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JToggleButton accountingButton;
    private javax.swing.JTabbedPane accountingTabs;
    private javax.swing.JToggleButton actionsButton;
    private javax.swing.JTabbedPane actionsTabs;
    private javax.swing.JButton applyButton;
    private javax.swing.JPanel buttonPanel;
    private javax.swing.JButton cancelButton;
    private javax.swing.ButtonGroup categoriesButtonGroup;
    private javax.swing.JPanel categoriesPanel;
    private javax.swing.JToolBar categoriesToolbar;
    private javax.swing.JPanel commandPanel;
    private javax.swing.JToggleButton generalButton;
    private javax.swing.JTabbedPane generalTabs;
    private javax.swing.JButton helpButton;
    private javax.swing.JToggleButton interfaceButton;
    private javax.swing.JTabbedPane interfaceTabs;
    private javax.swing.JSeparator jSeparator1;
    private javax.swing.JToggleButton miscButton;
    private javax.swing.JTabbedPane miscTabs;
    private javax.swing.JButton okButton;
    private javax.swing.JTabbedPane reportingTabs;
    private javax.swing.JToggleButton reportsButton;
    // End of variables declaration//GEN-END:variables
}
