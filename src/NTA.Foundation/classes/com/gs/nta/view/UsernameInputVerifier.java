/*
 * Copyright (C) 2022 PekinSOFT Systems
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * *****************************************************************************
 * *****************************************************************************
 *  Project    :   NTA-Basic
 *  Class      :   UsernameInputVerifier.java
 *  Author     :   Sean Carrick
 *  Created    :   Jan 25, 2022 @ 10:49:13 AM
 *  Modified   :   Jan 25, 2022
 *  
 *  Purpose:
 *  
 *  Revision History:
 *  
 *  WHEN          BY                  REASON
 *  ----------    ------------------- ------------------------------------------
 *  Jan 25, 2022       Sean Carrick             Initial creation.
 * *****************************************************************************
 */

package com.gs.nta.view;

import javax.swing.InputVerifier;
import javax.swing.JComponent;
import javax.swing.JTextField;

/**
 *
 * @author Sean Carrick &lt;sean at pekinsoft dot com&gt;
 * 
 * @version 0.1.0
 * @since 0.1.0
 */
public class UsernameInputVerifier extends InputVerifier {

    public UsernameInputVerifier () {
        
    }

    @Override
    public boolean verify(JComponent input) {
        return ((JTextField) input).getText() != null
                && !((JTextField) input).getText().isBlank()
                && !((JTextField) input).getText().isEmpty()
                && ((JTextField) input).getText().length() > 3;
    }

}
