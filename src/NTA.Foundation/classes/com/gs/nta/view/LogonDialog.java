/*
 * Copyright (C) 2022 PekinSOFT Systems
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * *****************************************************************************
 *  Project    :   NTA-Basic
 *  Class      :   LogonDialog.java
 *  Author     :   Sean Carrick
 *  Created    :   Jan 25, 2022 @ 4:32:58 PM
 *  Modified   :   Jan 25, 2022
 *  
 *  Purpose: See class JavaDoc comments.
 *  
 *  Revision History:
 *  
 *  WHEN          BY                  REASON
 *  ------------  ------------------- ------------------------------------------
 *  Jan 25, 2022  Sean Carrick        Initial creation.
 * *****************************************************************************
 */
package com.gs.nta.view;

import com.gs.api.enums.SysExits;
import com.gs.application.Action;
import com.gs.application.ResourceMap;
import com.gs.nta.NTApp;
import com.gs.spi.logging.Level;
import java.awt.Toolkit;
import java.awt.event.KeyEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.IOException;
import java.util.EventObject;
import javax.swing.ActionMap;

/**
 *
 * @author Sean Carrick
 */
public class LogonDialog extends javax.swing.JDialog {
    
    private final NTApp app;
    private final ResourceMap resMap;
    private final ActionMap actMap;
    private boolean dataValid;

    /**
     * Creates new form LogonDialog
     * @param app
     * @param parent
     * @param modal
     */
    public LogonDialog(NTApp app, java.awt.Frame parent, boolean modal) {
        super(parent, modal);
        this.app = app;
        resMap = app.getContext().getResourceMap(getClass());
        actMap = app.getContext().getActionMap(this);
        
        initComponents();
    }
    
    @Action
    public void cancel() {
        storePosition();
        app.exit(new EventObject(this), SysExits.EX_OPCANCEL);
    }
    
    @Action(enabledProperty = "dataValid")
    public void logon() {
        app.setRuntimeProperty("current.user", usernameField.getText());
        app.setRuntimeProperty("current.password", passwordField.getPassword());
        storePosition();
        app.hide(this);
    }
    
    private void storePosition() {
        app.setSystemProperty("logon.window.x", getX());
        app.setSystemProperty("logon.window.y", getY());
    }
    
    public boolean isDataValid() {
        return dataValid;
    }
    
    public final void setDataValid(boolean dataValid) {
        boolean oldValue = isDataValid();
        this.dataValid = usernameField.getInputVerifier().verify(usernameField) 
                && passwordField.getInputVerifier().verify(passwordField);
        
        firePropertyChange("dataValid", oldValue, isDataValid());
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        infoPanel = new javax.swing.JPanel();
        instructionsLabel = new javax.swing.JLabel();
        commandPanel = new javax.swing.JPanel();
        jSeparator1 = new javax.swing.JSeparator();
        cancelButton = new javax.swing.JButton();
        logonButton = new javax.swing.JButton();
        passwordLabel = new javax.swing.JLabel();
        usernameLabel = new javax.swing.JLabel();
        usernameField = new javax.swing.JTextField();
        passwordField = new javax.swing.JPasswordField();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setName("LogonDialog"); // NOI18N
        setResizable(false);

        infoPanel.setBorder(javax.swing.BorderFactory.createTitledBorder("Logon Information"));
        infoPanel.setName("infoPanel"); // NOI18N

        instructionsLabel.setText("jLabel1");
        instructionsLabel.setVerticalAlignment(javax.swing.SwingConstants.TOP);
        instructionsLabel.setName("instructionsLabel"); // NOI18N

        javax.swing.GroupLayout infoPanelLayout = new javax.swing.GroupLayout(infoPanel);
        infoPanel.setLayout(infoPanelLayout);
        infoPanelLayout.setHorizontalGroup(
            infoPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(instructionsLabel, javax.swing.GroupLayout.PREFERRED_SIZE, 376, javax.swing.GroupLayout.PREFERRED_SIZE)
        );
        infoPanelLayout.setVerticalGroup(
            infoPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(instructionsLabel, javax.swing.GroupLayout.DEFAULT_SIZE, 147, Short.MAX_VALUE)
        );

        commandPanel.setName("commandPanel"); // NOI18N

        jSeparator1.setName("jSeparator1"); // NOI18N

        cancelButton.setText("jButton1");
        cancelButton.setName("cancelButton"); // NOI18N
        cancelButton.setAction(actMap.get("cancel"));
        cancelButton.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                trapEscape(evt);
            }
        });

        logonButton.setText("jButton2");
        logonButton.setName("logonButton"); // NOI18N
        logonButton.setAction(actMap.get("logon"));
        logonButton.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                trapEscape(evt);
            }
        });

        javax.swing.GroupLayout commandPanelLayout = new javax.swing.GroupLayout(commandPanel);
        commandPanel.setLayout(commandPanelLayout);
        commandPanelLayout.setHorizontalGroup(
            commandPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jSeparator1, javax.swing.GroupLayout.PREFERRED_SIZE, 400, javax.swing.GroupLayout.PREFERRED_SIZE)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, commandPanelLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(logonButton)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(cancelButton)
                .addContainerGap())
        );
        commandPanelLayout.setVerticalGroup(
            commandPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(commandPanelLayout.createSequentialGroup()
                .addComponent(jSeparator1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(commandPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(cancelButton)
                    .addComponent(logonButton))
                .addContainerGap())
        );

        passwordLabel.setText("Password:");
        passwordLabel.setName("passwordLabel"); // NOI18N
        passwordLabel.setText(resMap.getString("passwordLabel.text"));

        usernameLabel.setText("Username:");
        usernameLabel.setName("usernameLabel"); // NOI18N
        usernameLabel.setText(resMap.getString("usernameLabel.text"));

        if (app.getProperties().getPropertyAsString(NTApp.CURRENT_USER) != null
            && !app.getProperties().getPropertyAsString(NTApp.CURRENT_USER).isBlank()
            && !app.getProperties().getPropertyAsString(NTApp.CURRENT_USER).isEmpty()) {
            usernameField.setText(app.getProperties().getPropertyAsString(NTApp.CURRENT_USER));
        }
        usernameField.setName("usernameField"); // NOI18N
        usernameField.setInputVerifier(new UsernameInputVerifier());
        usernameField.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                selectText(evt);
            }
            public void focusLost(java.awt.event.FocusEvent evt) {
                deselectText(evt);
            }
        });
        usernameField.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                trapEscape(evt);
            }
            public void keyTyped(java.awt.event.KeyEvent evt) {
                textChanged(evt);
            }
        });

        passwordField.setName("passwordField"); // NOI18N
        passwordField.setInputVerifier(new PasswordInputVerifier());
        passwordField.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                selectText(evt);
            }
            public void focusLost(java.awt.event.FocusEvent evt) {
                deselectText(evt);
            }
        });
        passwordField.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                trapEscape(evt);
            }
            public void keyTyped(java.awt.event.KeyEvent evt) {
                textChanged(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(commandPanel, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(infoPanel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(passwordLabel)
                            .addComponent(usernameLabel))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(usernameField)
                            .addComponent(passwordField))))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(infoPanel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(usernameLabel)
                    .addComponent(usernameField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(passwordLabel)
                    .addComponent(passwordField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addComponent(commandPanel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
        );

        setDataValid(false);

        getRootPane().setDefaultButton(logonButton);
        getRootPane().addKeyListener(new java.awt.event.KeyAdapter() {
            @Override
            public void keyReleased(java.awt.event.KeyEvent evt) {
                trapEscape(evt);
            }
        }
    );

    addWindowListener(new LogonDialogAdapter());

    int scrWidth = Toolkit.getDefaultToolkit().getScreenSize().width;
    int scrHeight = Toolkit.getDefaultToolkit().getScreenSize().height;

    int x = (app.getProperties().getPropertyAsInteger("logon.window.x",
        (scrWidth - getWidth()) / 2));
int y = (app.getProperties().getPropertyAsInteger("logon.window.y",
    (scrHeight - getHeight()) / 2));

    setLocation(x, y);

    pack();
    }// </editor-fold>//GEN-END:initComponents

    private void selectText(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_selectText
        if (evt.getSource() instanceof javax.swing.text.JTextComponent) {
            javax.swing.text.JTextComponent c = (javax.swing.text.JTextComponent) evt.getSource();
            c.selectAll();
        }
    }//GEN-LAST:event_selectText

    private void deselectText(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_deselectText
        if (evt.getSource() instanceof javax.swing.text.JTextComponent) {
            javax.swing.text.JTextComponent c = (javax.swing.text.JTextComponent) evt.getSource();
            c.select(0, 0);
        }
    }//GEN-LAST:event_deselectText

    private void textChanged(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_textChanged
        setDataValid(true);
    }//GEN-LAST:event_textChanged

    private void trapEscape(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_trapEscape
        if (evt.getKeyCode() == KeyEvent.VK_ESCAPE) {
            cancel();
        }
    }//GEN-LAST:event_trapEscape

    private class LogonDialogAdapter extends WindowAdapter {

        @Override
        public void windowClosing(WindowEvent e) {
            try {
                app.getContext().getSessionStorage().save(LogonDialog.this, 
                        getClass().getName() + "-session.xml");
            } catch (IOException ex) {
                String message = String.format("Unable to save session state for %s", 
                        this);
                app.getLogger().log(Level.WARN, message);
            }
        }
        
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton cancelButton;
    private javax.swing.JPanel commandPanel;
    private javax.swing.JPanel infoPanel;
    private javax.swing.JLabel instructionsLabel;
    private javax.swing.JSeparator jSeparator1;
    private javax.swing.JButton logonButton;
    private javax.swing.JPasswordField passwordField;
    private javax.swing.JLabel passwordLabel;
    private javax.swing.JTextField usernameField;
    private javax.swing.JLabel usernameLabel;
    // End of variables declaration//GEN-END:variables
}
