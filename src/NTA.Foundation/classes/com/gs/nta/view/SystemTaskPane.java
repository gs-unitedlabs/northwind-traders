/*
 * Copyright (C) 2022 PekinSOFT Systems
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * *****************************************************************************
 *  Project    :   NTA-Basic
 *  Class      :   SystemTaskPane.java
 *  Author     :   Sean Carrick
 *  Created    :   Jan 25, 2022 @ 5:52:30 PM
 *  Modified   :   Jan 25, 2022
 *  
 *  Purpose: See class JavaDoc comments.
 *  
 *  Revision History:
 *  
 *  WHEN          BY                  REASON
 *  ------------  ------------------- ------------------------------------------
 *  Jan 25, 2022  Sean Carrick        Initial creation.
 * *****************************************************************************
 */

package com.gs.nta.view;

import com.gs.application.Action;
import com.gs.application.Application;
import com.gs.application.ResourceMap;
import com.gs.nta.NTApp;
import com.gs.spi.desktop.TaskPaneProvider;
import com.gs.spi.persistence.SaveCookie;
import com.gs.utils.MessageBox;
import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import javax.swing.ActionMap;
import javax.swing.JSeparator;
import org.jdesktop.swingx.JXTaskPane;

/**
 *
 * @author Sean Carrick &lt;sean at pekinsoft dot com&gt;
 * 
 * @version 0.1.0
 * @since 0.1.0
 */
public class SystemTaskPane implements TaskPaneProvider, SaveCookie {
    
    public static final String SAVE_NEEDED = "saveNeeded";
    
    private final NTApp app;
    private final ResourceMap resMap;
    private final ActionMap actMap;
    private final PropertyChangeSupport pcs;
    private final JXTaskPane tasks = new JXTaskPane();
    
    private boolean saveNeeded;
    
    public SystemTaskPane () {
        app = (NTApp) Application.getInstance();
        resMap = app.getContext().getResourceMap(SystemTaskPane.class);
        actMap = app.getContext().getActionMap(this);
        pcs = app.getPropertyChangeSupport();
        tasks.setName("systemTasks");
        
        saveNeeded = false;
    }

    @Override
    public JXTaskPane getTaskPane() {
        tasks.add(actMap.get("showOptions"));
        tasks.add(new JSeparator());
        tasks.add(actMap.get("showAbout"));
        tasks.add(actMap.get("showHelp"));
        tasks.add(new JSeparator());
        tasks.add(actMap.get("save"));
        tasks.add(new JSeparator());
        tasks.add(actMap.get("quit"));
        
        return tasks;
    }
    
    public void addPropertyChangeListener(PropertyChangeListener listener) {
        tasks.addPropertyChangeListener(listener);
    }
    
    public void addPropertyChangeListener(String propertyName, 
            PropertyChangeListener listener) {
        tasks.addPropertyChangeListener(propertyName, listener);
    }
    
    public void removePropertyChangeListener(PropertyChangeListener listener) {
        tasks.removePropertyChangeListener(listener);
    }
    
    public void removePropertyChangeListener(String propertyName, 
            PropertyChangeListener listener) {
        tasks.removePropertyChangeListener(propertyName, listener);
    }
    
    public boolean isSaveNeeded() {
        return app.isSaveNeeded();
    }
    
    public void setSaveNeeded(boolean saveNeeded) {
        app.getPropertyChangeSupport().firePropertyChange("saveNeeded", 
                saveNeeded, isSaveNeeded());
        saveNeeded = isSaveNeeded();
    }
    
    @Action(enabledProperty = "saveNeeded")
    @Override
    public void save() {
        MessageBox.showInfo("Save Completed!", "Saved");
    }

}
