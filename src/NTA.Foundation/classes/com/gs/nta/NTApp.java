/*
 * Copyright (C) 2022 PekinSOFT Systems
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * *****************************************************************************
 *  Project    :   NTA-Basic
 *  Class      :   NTApp.java
 *  Author     :   Sean Carrick
 *  Created    :   Jan 26, 2022 @ 5:49:41 PM
 *  Modified   :   Jan 26, 2022
 *  
 *  Purpose: See class JavaDoc comments.
 *  
 *  Revision History:
 *  
 *  WHEN          BY                  REASON
 *  ------------  ------------------- ------------------------------------------
 *  Jan 26, 2022  Sean Carrick        Initial creation.
 * *****************************************************************************
 */

package com.gs.nta;

import com.gs.api.enums.SysExits;
import com.gs.api.logging.LogRecord;
import com.gs.api.properties.Properties;
import com.gs.application.Application;
import com.gs.nta.view.LogonDialog;
import com.gs.nta.view.MainFrame;
import com.gs.spi.bugs.BugReporter;
import com.gs.spi.logging.Level;
import com.gs.spi.modules.Module;
import com.gs.spi.persistence.SaveCookie;
import com.gs.utils.MessageBox;
import java.io.File;
import java.io.IOException;
import java.time.Instant;
import java.util.ArrayList;
import java.util.EventObject;
import java.util.Iterator;
import java.util.List;
import java.util.ServiceLoader;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JInternalFrame;
import org.apache.derby.drda.NetworkServerControl;

/**
 *
 * @author Sean Carrick &lt;sean at pekinsoft dot com&gt;
 * 
 * @version 0.1.0
 * @since 0.1.0
 */
public class NTApp extends Application {
    
    private MainFrame mainFrame;
    
    private Properties props;
    private List<SaveCookie> saveCookies;
    private List<Module> modules;
    private NetworkServerControl server;
    
    public NTApp () {
    }
    
    public void setRuntimeProperty(String propertyName, Object value) {
        props.setRuntimeProperty(propertyName, value);
    }
    
    public void setSystemProperty(String propertyName, Object value) {
        props.setSystemProperty(propertyName, value);
    }
    
    public Object getProperty(String propertyName) {
        return props.getProperty(propertyName);
    }
    
    public Object getProperty(String propertyName, Object defaultValue) {
        return props.getProperty(propertyName, defaultValue);
    }
    
    public Properties getProperties() {
        return props;
    }
    
    public File getLogDirectory() {
        String logPath = getContext()
                .getLocalStorage()
                .getDirectory()
                .getAbsolutePath();
        
        if (!logPath.endsWith(File.separator)) {
            logPath += File.separator;
        }
        
        logPath += "var" + File.separator + "log" + File.separator;
        
        File logDir = new File(logPath);
        if (!logDir.exists()) {
            try {
                logDir.mkdirs();
            } catch (SecurityException ex) {
                String msg = "Unable to create the log directory: " 
                        + logDir.getAbsolutePath()
                        + "\nCheck that you have permissions to write in this "
                        + "location.";
                MessageBox.showError(ex, msg, 
                        getContext()
                                .getResourceMap()
                                .getString("Application.title"));
                logger.log(Level.ERROR, msg + "\nEXITING!");
                exit(new EventObject(this), SysExits.EX_NOPERM);
            }
        }
        
        return logDir;
    }
    
    public List<Module> getInstalledModules() {
        List<Module> copy = new ArrayList<>();
        
        modules.forEach(m -> {
            copy.add(m);
        });
        
        return copy;
    }

    @Override
    protected void initialize(String[] args) {
        logger.enter(Level.TRACE, "Initializing the application...");
        props = new Properties();
        props.loadProperties();
        modules = new ArrayList<>();
        saveCookies = new ArrayList<>();
        
        logger.log(Level.CONFIG, "Parsing the command line arguments...");
        Parser parser = new Parser(props);
        parser.parse(args);
        logger.log(Level.DEBUG, "Command line argument have been parsed and the "
                + "properties have been loaded.");
        
        logger.log(Level.CONFIG, "Loading modules...");
        loadModules();
        
        logger.log(Level.CONFIG, "Starting the database server...");
        startDatabaseServer();
        
        logger.exit(Level.TRACE, "Application successfully initialized.");
    }

    @Override
    protected void startup() {
        mainFrame = new MainFrame(this);

        LogonDialog dlg = new LogonDialog(this, mainFrame, true);
        getContext().getResourceMap(LogonDialog.class).injectComponents(dlg);
        show(dlg);
        try {
            getContext().getSessionStorage().restore(dlg, dlg.getName() 
                    + ".session.xml");
        } catch (IOException ex) {
            logger.log(Level.WARN, "Unable to restore session state for "
                    + dlg.getName());
        }
        
        getContext().getResourceMap().injectComponents(getMainFrame());
        show(mainFrame);
    }

    @Override
    protected void ready() {
        // TODO: Add implementation to NTApp.ready
        super.ready();
    }
    
    public MainFrame getMainFrame() {
        return mainFrame;
    }
    
    public void show(JDialog d) {
        try {
            getContext().getResourceMap(d.getClass()).injectComponents(d);
            getContext().getSessionStorage().restore(d, d.getName() + ".session.xml");
            d.setVisible(true);
        } catch (IOException ex) {
            logger.log(Level.WARN, String.format("Unable to restore session state "
                    + "for %s\nReason: %s", d, ex));
        }
    }
    
    public void show(JFrame d) {
        try {
            getContext().getResourceMap(d.getClass()).injectComponents(d);
            getContext().getSessionStorage().restore(d, d.getName() + ".session.xml");
            d.setVisible(true);
        } catch (IOException ex) {
            logger.log(Level.WARN, String.format("Unable to restore session state "
                    + "for %s\nReason: %s", d, ex));
        }
    }
    
    public void show(JInternalFrame d) {
        try {
            getContext().getResourceMap(d.getClass()).injectComponents(d);
            getContext().getSessionStorage().restore(d, d.getName() + ".session.xml");
            d.setVisible(true);
        } catch (IOException ex) {
            logger.log(Level.WARN, String.format("Unable to restore session state "
                    + "for %s\nReason: %s", d, ex));
        }
    }
    
    public void hide(JDialog d) {
        try {
            getContext().getSessionStorage().save(d, d.getName() 
                    + ".session.xml");
        } catch (IOException ex) {
            logger.log(Level.WARN, String.format("Unable to save session state "
                    + "for %s\nReason: %s", d, ex));
        }
        d.setVisible(false);
        d.dispose();
    }
    
    public void hide(JFrame f) {
        try {
            getContext().getSessionStorage().save(f, f.getName() 
                    + ".session.xml");
        } catch (IOException ex) {
            logger.log(Level.WARN, String.format("Unable to save session state "
                    + "for %s\nReason: %s", f, ex));
        }
        f.setVisible(false);
        f.dispose();
    }
    
    public void hide(JInternalFrame f) {
        try {
            getContext().getSessionStorage().save(f, f.getName() 
                    + ".session.xml");
        } catch (IOException ex) {
            logger.log(Level.WARN, String.format("Unable to save session state "
                    + "for %s\nReason: %s", f, ex));
        }
        f.setVisible(false);
        f.dispose();
    }

    @Override
    protected void shutdown() {
        // TODO: Add implementation to NTApp.shutdown
        super.shutdown();
        
        // Store the properties.
        props.storeProperties();
    }

    private void loadModules() {
        logger.enter(Level.TRACE, "Loading modules...");
        
        ServiceLoader<Module> loader = ServiceLoader.load(Module.class, 
                getClass().getClassLoader());
        Iterator<Module> it = (Iterator<Module>) loader.iterator();
        
        while (it.hasNext()) {
            modules.add(it.next());
        }
        
        logger.log(Level.CONFIG, "Loaded modules:");
        modules.forEach(m -> {
            logger.log(Level.CONFIG, "\tModule: " + m.getPluginName() 
                    + " [" + m.getVersion() + "]");
        });
        
        logger.exit(Level.TRACE, "Modules listed above have been loaded.");
    }
    
    private void startDatabaseServer() {
        String svrRTInfo = "";
        String svrSysInfo = "";
        try {

            server = new NetworkServerControl();
            
            server.start(null);
            svrRTInfo = server.getRuntimeInfo();
            svrSysInfo = server.getSysinfo();
        } catch (Exception ex) {
            String msg = String.format("Unable to start database server: %s\n\n"
                    + "EXITING!", ex);
            LogRecord rec = new LogRecord(getClass().getName());
            rec.setInstant(Instant.now());
            rec.setLevel(Level.CRITICAL);
            rec.setMessage(msg);
            rec.setParameters(null);
            rec.setSourceMethodName("initialize");
            rec.setThread(Thread.currentThread());
            rec.setThrown(ex);
            logger.log(rec); // Log the error.
            
            // Prepare the Bug Reporter...
            ServiceLoader<BugReporter> l = ServiceLoader.load(BugReporter.class);
            BugReporter rpt = (BugReporter) l.iterator().next(); // Get the 1st.
            rpt.setErrFileName(null);
            rpt.setLogFileName(getLogDirectory().getAbsolutePath() 
                    + File.separator + "NTA-Basic.log");
            rpt.setLogRecord(rec);
            
            // Show the Bug Report Wizard...
            rpt.showReport();
            
            // Exit the application because it's useless without a database.
            exit(null, SysExits.EX_NOHOST);
        }
        logger.log(Level.CONFIG, svrRTInfo);
        logger.log(Level.CONFIG, svrSysInfo);
    }

    /* *************** Common Application Property Names *************** */
    /**
     * Property name constant for the currently logged in user's username. This
     * value may be used for such things as are required by the application,
     * such as placing it in the status or title bar of the application,
     * maintaining some kind of records accounting system in the database, etc.
     * This value should be stored to the system properties at shutdown so that
     * if the same user logs on the next time the application is run, they will
     * not need to type in their username again, just their password.
     */
    public static final String CURRENT_USER = "current.user";

    /**
     * Property name constant for the currently logged in user's encrypted
     * password hash. This is only stored so that when the application shuts
     * down, it may be saved to the system properties for use at the next logon.
     */
    public static final String CURRENT_PASS = "current.password";

    /**
     * Property name constant for the last logged in user's username. This is to
     * allow an application to preset the last logged in username to the logon
     * dialog's username field, so that the user doesn't have to retype his/her
     * username every time they run the application. This is just used as a
     * convenience for the user, as they will still need to type in their
     * password.
     */
    public static final String LAST_USER = "last.user";

    /**
     * Property name constant for storing the encrypted hash of the last
     * password used to log into the application. The password is never stored
     * in clear text and uses a very secure algorithm for encrypting the
     * provided password. Further, the encryption used is one-way, meaning that
     * it cannot be decrypted. Therefore, when a password is entered again, it
     * must be encrypted, then have that hash compared to the last password hash
     * in order to see if they match.
     */
    public static final String LAST_PASS = "last.password";

    /**
     * Property name constant for the logging level for application log files.
     * The logging level set for this property should be one of the constants in
     * `com.gs.spi.logging.Level` enumeration. The set level determines the
     * verbosity of the logging output written to the log file. The output that
     * is written to the console only cares whether or not logging is turned on.
     * Beyond that, console output is always at the most verbose level, as most
     * people will not ever see that logging output. However, it is beneficial
     * to developers, as the log messages will all be output to the output
     * window of their IDE, allowing them to see everything that is going on
     * within the application (provided log messages have been sent to the
     * logger). Yet, once the project moves to a production environment, those
     * messages will no longer be seen unless the application is launched from a
     * terminal.
     */
    public static final String LOG_LEVEL = "logging.level";

    /**
     * Property name constant for the formatting of log files for the
     * application. This should be a boolean setting where the value `true`
     * means that the application log file output should be formatted (for
     * printing for example), and `false` means that no additional formatting
     * beyond that provided by the log message will be applied.
     */
    public static final String LOG_FORMAT = "logging.formatted";

    /**
     * Property name constant for the mode in which the application is running.
     * This should be a boolean setting where the value `true` means that the
     * application is running in the development mode, and `false` means that
     * the application is running in a production mode (environment).
     *
     * This property can be used to help determine the logging level for the
     * application's log files.
     */
    public static final String APP_RUN_MODE = "application.running.mode";

    /**
     * Property name constant for the default Look and Feel for the application.
     * While the application maintains a look and feel in the resource
     * properties file, there is no way to write back to that file if the user
     * changes the look and feel of the application. Therefore, the look and
     * feel will need to be set in the application's system properties to
     * maintain it from one run of the application to the next.
     *
     * At application startup, the look and feel property should be tested
     * *after* the main frame's properties have been injected to see if there is
     * a look and feel stored. If there is, then that look and feel should be
     * applied to the UI.
     */
    public static final String APP_LAF = "application.default.lookAndFeel";

    /**
     * Property name constant for the URL to the database server for the
     * application. While not all applications based on the
     * `PlatformApplication` super class will need this, enough will that it has
     * been made available for use.
     */
    public static final String DB_URL = "database.url";

    /**
     * Property name constant for the name of the database for the application.
     * While not all applications based on the `PlatformApplication` super class
     * will need this, enough will that it has been made available for use.
     */
    public static final String DB_NAME = "database.name";

}
