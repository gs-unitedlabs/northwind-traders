/*
 * Copyright (C) 2022 PekinSOFT Systems
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * *****************************************************************************
 * *****************************************************************************
 *  Project    :   NTA-Basic
 *  Class      :   DatabaseCreator.java
 *  Author     :   Sean Carrick
 *  Created    :   Jan 24, 2022 @ 9:54:33 AM
 *  Modified   :   Jan 24, 2022
 *  
 *  Purpose:
 *  
 *  Revision History:
 *  
 *  WHEN          BY                  REASON
 *  ----------    ------------------- ------------------------------------------
 *  Jan 24, 2022       Sean Carrick             Initial creation.
 * *****************************************************************************
 */

package com.gs.nta;

import com.gs.application.Application;
import com.gs.spi.database.FieldSpecification;
import com.gs.spi.database.TableSpecification;
import com.gs.spi.logging.Level;
import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 *
 * @author Sean Carrick &lt;sean at pekinsoft dot com&gt;
 * 
 * @version 0.1.0
 * @since 0.1.0
 */
public class DerbyDatabaseCreator {
    
    private final NTApp app;
    private final String dbURL;
    private final String dbName;
    private final String username;
    private final char[] password;
    
    private Connection conn;
    private Statement stmt;
    
    /**
     * Constructs a new instance of the `DatabaseCreator` class to create the
     * database and all specified tables.
     * 
     * @param app the `Application` to which this database belongs
     * @param dbURL the URL for the database (defaults to &ldquo;localhost:1527&rdquo;)
     * @param dbName database name
     * @param username the username of the logged in user (will become db owner)
     * @param password user's password (will become db owner password)
     * @throws IllegalArgumentException if `app`, `dbName`, `username`, or 
     * `password` are `null`, blank, or empty
     */
    public DerbyDatabaseCreator (final Application app, String dbURL, 
            final String dbName, final String username, final char[] password) {
        
        // Validate required parameters
        if (app == null) {
            throw new IllegalArgumentException("null app");
        }
        if (dbName == null || dbName.isBlank() || dbName.isEmpty()) {
            throw new IllegalArgumentException("null, blank, or empty dbName");
        }
        if (username == null || username.isBlank() || username.isEmpty()) {
            throw new IllegalArgumentException("null, blank, or empty username");
        }
        if (password == null || password.length == 0) {
            throw new IllegalArgumentException("null or empty password");
        }
        
        // Validate the supplied URL. If it is null, blank, or empty, set it.
        if (dbURL == null || dbURL.isBlank() || dbURL.isEmpty()) {
            dbURL = "jdbc:derby:localhost/";
            // If it is not null blank or empty, check that it starts with the
            //+ appropriate Derby connection string, or set it if it doesn't
        } else if (!dbURL.startsWith("jdbc:derby:")) {
            dbURL = "jdbc:derby:" + dbURL;
        }
        // Make sure the Derby URL ends with a slash, so that we can just add
        //+ the database name and parameters in the connection method.
        if (!dbURL.endsWith("/")) {
//            dbURL += "/";
        }
        
        this.app = (NTApp) app;
        this.dbURL = dbURL;
        this.dbName = dbName;
        this.username = username;
        this.password = password;
    }
    
    protected void connect() throws SQLException, ClassNotFoundException {
        DriverManager.registerDriver(new org.apache.derby.jdbc.ClientDriver());
        conn = DriverManager.getConnection(dbURL + dbName + ";create=true;",
                username, String.copyValueOf(password));
    }
    
    /**
     * Creates the database, the tables, and any foreign key relationships that
     * may be defined. The database that is created will have the name specified
     * when this instance of `DatabaseCreator` was constructed, and the supplied
     * user will become the owner of the database.
     * 
     * @param tables the tables to create
     * @param develMode if `develMode == true`, then the tables will be dropped
     * and recreated; otherwise, only new tables will be created
     * @throws SQLException in the event a database error occurs; non-existent
     * tables for foreign key relationships will be dealt with, provided the
     * referenced table exists in the list
     * @throws ClassNotFoundException in the event the driver cannot be located
     * @throws IllegalArgumentException in the event that the provided `tables`
     * list is `null` or empty
     */
    public void createDatabase(List<TableSpecification> tables, boolean develMode) 
            throws SQLException, ClassNotFoundException {
        if (tables == null || tables.isEmpty()) {
            throw new IllegalArgumentException("null or empty tables list");
        }
        
        // Make the connection to the database server:
        connect();
        
        // First, we are going to see if we are in development mode. If so, we
        //+ will drop all tables from the database, if they exist.
        if (develMode) {
            for (TableSpecification t : tables) {
                if (doesTableExist(t.getTableName())) {
                    dropTable(t.getTableName());
                }
            }
        }
        
        // A list of tables that reference other tables, for later creation
        List<TableSpecification> fkTables = new ArrayList<>();
        
        for (TableSpecification t : tables) {
            if (doesTableExist(t.getTableName())) {
                // Table exists, so move on to the next table.
                continue;
            }
            
            // Since the table does not exist, we need to see if it has a 
            //+ foreign key.
            if (!t.getForeignKeys().isEmpty()) {
                // Since there is a (are some) foreign key(s), we need to see if
                //+ the referenced table(s) exist(s) already.
                Set<String> keys = t.getForeignKeysKeySet();
                for (String key : keys) {
                    // Perform the check to see if the referenced table(s) exist(s)
                    if (!doesTableExist(t.getForeignKeys().get(key))) {
                        // If any of the referenced tables do not exist...
                        //+ ...store the table in the fkTables list...
                        fkTables.add(t);
                    }
                }
                //+ ...and bail out to the next table.
                continue;
            }
            
            // Now that we have checked if the table exists, and that, if it has
            //+ foreign key relationships that the referenced table(s) exist or
            //+ the table has been added to our fkTables list, we can add the
            //+ current (surviving) table to our database.
            addTable(t);
        } // Loop complete
        
        // Close the connection. It will be reopened if there are foreign key
        //+ tables to add.
        conn.close();
        
        // Check to see if we have any tables that contained at least one
        // foreign key that did not exist...
        if (!fkTables.isEmpty()) {
            // ...recurse to add these tables...
            createDatabase(fkTables, develMode);
        } // Otherwise, we are done and the database has been created.
    }
    
    /**
     * Performs the creation of a single table. This method is separated out so
     * that we can have logic in the `createDatabase` method to create free-standing
     * tables first, then create the foreign key relationship tables that 
     * reference the other tables.
     * 
     * @param table the table to be created
     * @throws SQLException in the event a database error occurs
     * @throws ClassNotFoundException in the event the driver cannot be located
     */
    protected void addTable(TableSpecification table) throws SQLException, ClassNotFoundException {
        StringBuilder sql = new StringBuilder();
        StringBuilder idx = new StringBuilder();
        StringBuilder fks = new StringBuilder();
        
        sql.append("CREATE TABLE ").append(table.getTableName());
        sql.append("(\n");
        
        for (FieldSpecification f : table.getFields()) {
            sql.append("\t");
            sql.append(f.getName());
            sql.append(" ");
            
            switch (f.getDataType()) {
                case BIGINT:
                    sql.append("BIGINT");
                    break;
                case BLOB:
                    sql.append("BLOB(").append(f.getDataLength()).append(")");
                    break;
                case BOOLEAN:
                    sql.append("BOOLEAN");
                    break;
                case CHAR:
                    sql.append("CHAR (").append(f.getDataLength()).append(")");
                    break;
                case CHAR_FOR_BIT_DATA:
                    sql.append("CHAR(").append(f.getDataLength()).append(") ");
                    sql.append("FOR BIT DATA");
                    break;
                case CLOB:
                    sql.append("CLOB(").append(f.getDataLength()).append(")");
                    break;
                case DATE:
                    sql.append("DATE");
                    break;
                case DECIMAL:
                    sql.append("DECIMAL(").append(f.getDataLength()).append(")");
                    break;
                case DOUBLE:
                    sql.append("DOUBLE");
                    break;
                case DOUBLE_PRECISION:
                    sql.append("DOUBLE PRECISION");
                    break;
                case FLOAT:
                    sql.append("FLOAT");
                    if (f.getDataLength() != null && !f.getDataLength().isBlank()
                            && !f.getDataLength().isEmpty()) {
                        sql.append("(").append(f.getDataLength()).append(")");
                    }
                    break;
                case INTEGER:
                    sql.append("INTEGER");
                    break;
                case LONG_VARCHAR:
                    sql.append("LONG VARCHAR");
                    break;
                case LONG_VARCHAR_FOR_BIT_DATA:
                    sql.append("LONG VARCHAR FOR BIT DATA");
                    break;
                case NUMERIC:
                    sql.append("NUMERIC(").append(f.getDataLength()).append(")");
                    break;
                case REAL:
                    sql.append("REAL");
                    break;
                case SMALLINT:
                    sql.append("SMALLINT");
                    break;
                case TIME:
                    sql.append("TIME");
                    break;
                case TIMESTAMP:
                    sql.append("TIMESTAMP");
                    break;
                case VARCHAR:
                    sql.append("VARCHAR(").append(f.getDataLength()).append(")");
                    break;
                case VARCHAR_FOR_BIT_DATA:
                    sql.append("VARCHAR(").append(f.getDataLength()).append(") ");
                    sql.append("FOR BIT DATA");
                    break;
                default:
                    throw new IllegalArgumentException("Invalid data type. Needs "
                        + "to be one of the DataTypes constants.");
            }
            
            if (f.isPrimaryKey()) {
                sql.append(" PRIMARY KEY NOT NULL");
                
                if (f.isAutoIncrementing()) {
                    sql.append(" GENERATEd ALWAYS AS IDENTITY (START WITH 1, INCREMENT BY 1)");
                }
            }
            
            if (f.isUnique()) {
                sql.append(" UNIQUE");
            }
            
            if (!f.isNullAllowed()) {
                sql.append(" NOT NULL");
            }
            
            if (f.getDefaultValue() != null) {
                if (f.getDefaultValue() instanceof String) {
                    sql.append(" DEFAULT '");
                    sql.append(f.getDefaultValue().toString());
                    sql.append("'");
                } else {
                    sql.append(" DEFAULT ");
                    sql.append(f.getDefaultValue());
                }
            }
            
            if (f.isIndexed()) {
                idx.append("INDEX(").append(f.getName()).append(")\n");
            }
            
            if (f.getForeignKeyTableAndField() != null) {
                String[] fk = f.getForeignKeyTableAndField().split(".");
                fks.append("CONSTRAINT FK_").append(f.getName());
                fks.append("\n\t\t FOREIGN KEY (").append(f.getName());
                fks.append(") REFERENCES (").append(fk[0]).append("(");
                fks.append(fk[1]).append(")\n");
            }
            sql.append(",\n");
        }
        
        if (!sql.toString().endsWith(" ")) {
            sql.append(" ");
        }
        
        sql.append(idx);
        
        if (!sql.toString().endsWith(" ")) {
            sql.append(" ");
        }
        
        sql.append(fks);
        
        String t = sql.toString();
        t = t.substring(0, t.lastIndexOf(",\n")) + "\n";
        sql = new StringBuilder();
        sql.append(t);
        if (!sql.toString().endsWith(")")) {
            sql.append(")");
        }
        
        // Make a connection if we are not already connected.
        if (conn.isClosed()) {
            connect();
        }
        
        app.getLogger().log(Level.TRACE, sql.toString());
        
        Statement s = conn.createStatement();
        s.execute(sql.toString());
        s.close();
        
    }
    
    private void dropTable(String table) throws SQLException, ClassNotFoundException {
        // Make a connection if we are not already connected.
        if (conn.isClosed()) {
            connect();
        }
        stmt = conn.createStatement();
        stmt.execute("DROP TABLE " + table);
        
        stmt.close();
    }
    
    private boolean doesTableExist(String tableName) throws ClassNotFoundException {
        boolean exists = false;
        
        try {
            // Make a connection if we are not already connected.
            if (conn.isClosed()) {
                connect();
            }
            Set<String> set = getDBTables(conn);
            
            exists = set.contains(tableName.toLowerCase());
        } catch (SQLException ex) {
            if (ex.getSQLState().equalsIgnoreCase("42x05")) {
                // The table already exists, so bail out to the next table.
                exists = false;
            }
        }
        
        return exists;
    }
    
    private Set<String> getDBTables(Connection dbConn) throws SQLException {
        Set<String> set = new HashSet<String>();
        DatabaseMetaData dbmeta = dbConn.getMetaData();
        readDbTable(set, dbmeta, "TABLE", null);
        readDbTable(set, dbmeta, "SET", null);
        return set;
    }
    
    private void readDbTable(Set<String> set, DatabaseMetaData dbmeta, 
            String searchCriteria, String schema) throws SQLException {
        ResultSet rs = dbmeta.getTables(null, schema, null, new String[] {searchCriteria});
        
        while(rs.next()) {
            set.add(rs.getString("TABLE_NAME").toLowerCase());
        }
    }

}
