/*
 * Copyright (C) 2022 PekinSOFT Systems
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * *****************************************************************************
 *  Project    :   NTA-Basic
 *  Class      :   FoundationModule.java
 *  Author     :   Sean Carrick
 *  Created    :   Jan 26, 2022 @ 8:11:31 AM
 *  Modified   :   Jan 26, 2022
 *  
 *  Purpose: See class JavaDoc comments.
 *  
 *  Revision History:
 *  
 *  WHEN          BY                  REASON
 *  ------------  ------------------- ------------------------------------------
 *  Jan 26, 2022  Sean Carrick        Initial creation.
 * *****************************************************************************
 */

package com.gs.nta;

import com.gs.application.Application;
import com.gs.nta.view.GeneralOptions;
import com.gs.nta.view.LookAndFeelOptions;
import com.gs.nta.view.SystemTaskPane;
import com.gs.spi.desktop.OptionsPanelProvider;
import com.gs.spi.desktop.TaskPaneProvider;
import com.gs.spi.modules.Module;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import javax.swing.ImageIcon;

/**
 *
 * @author Sean Carrick &lt;sean at pekinsoft dot com&gt;
 * 
 * @version 0.1.0
 * @since 0.1.0
 */
public class FoundationModule implements Module {

    @Override
    public List<OptionsPanelProvider> getOptionsPanel() {
        List<OptionsPanelProvider> options = new ArrayList<>();
        options.add(new GeneralOptions());
        options.add(new LookAndFeelOptions());
        
        return options;
    }

    @Override
    public TaskPaneProvider getTaskPane() {
        return new SystemTaskPane();
    }

    @Override
    public String getPluginName() {
        return "NTA Foundation";
    }

    @Override
    public String getCopyright() {
        return "Copyright© 2007-2023 " + getVendorName() + ". All rights under "
                + "copyright reserved.";
    }

    @Override
    public List<String> getContributors() {
        List<String> list = new ArrayList<>();
        list.add("Sean Carrick: Founding Partner, Lead Developer - Illinois, USA");
        list.add("Jiří Kovalský: Founding Partner, Lead Tester - Bohumín, Czech Republic");
        list.add("Kevin Nathan: Contributing Developer - Arizona, USA");
        
        return list;
    }

    @Override
    public String getDescription() {
        return "NTA Foundation is the base of the Northwind Traders Basic "
                + "Edition application. Without this module, there is no "
                + "Northwind Traders. This module provides the main window and "
                + "general system components, such as the About window and "
                + "Options dialog.";
    }

    @Override
    public ImageIcon getImageIcon() {
        return Application
                .getInstance()
                .getContext()
                .getResourceMap()
                .getImageIcon("Application.icon");
    }

    @Override
    public URL getWebsiteURL() throws MalformedURLException {
        return new URL("https://gs-unitedlabs.com");
    }

    @Override
    public int getAboutTabPosition() {
        return Integer.MIN_VALUE;
    }

}
