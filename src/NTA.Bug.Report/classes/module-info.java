/*
 * Copyright (C) 2022 GS United Labs
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * *****************************************************************************
 *  Project    :   NTA-Basic
 *  Class      :   module-info.java
 *  Author     :   Sean Carrick
 *  Created    :   Jan 19, 2022 @ 12:22:59 PM
 *  Modified   :   Jan 19, 2022
 *  
 *  Purpose: See class JavaDoc
 *  
 *  Revision History:
 *  
 *   WHEN          BY                  REASON
 *   ------------  ------------------- ------------------------------------------
 *  Jan 19, 2022  Sean Carrick        Initial creation.
 * *****************************************************************************
 */

open module NTA.Bug.Report {
    requires java.base;
    requires java.desktop;
    requires java.logging;
    
    requires gs.platform.api;
    requires NTA.Foundation;
    requires activation;
    
    exports com.gs.nta.bugs;
    
    provides com.gs.spi.bugs.BugReporter with
            com.gs.nta.bugs.BugReporter;
}
