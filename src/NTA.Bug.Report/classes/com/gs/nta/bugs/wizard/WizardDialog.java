/*
 * Copyright (C) 2022 GS United Labs
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * *****************************************************************************
 *  Project    :   NTA-Basic
 *  Class      :   WizardDialog.java
 *  Author     :   Sean Carrick
 *  Created    :   Jan 21, 2022 @ 6:08:40 PM
 *  Modified   :   Jan 21, 2022
 *  
 *  Purpose: See class JavaDoc
 *  
 *  Revision History:
 *  
 *   WHEN          BY                  REASON
 *   ------------  ------------------- ------------------------------------------
 *  Jan 21, 2022  Sean Carrick        Initial creation.
 * *****************************************************************************
 */
package com.gs.nta.bugs.wizard;

import com.gs.api.logging.LogRecord;
import com.gs.application.Action;
import com.gs.application.Application;
import com.gs.spi.modules.Module;
import com.gs.application.ResourceMap;
import com.gs.application.Task;
import com.gs.nta.NTApp;
import com.gs.spi.logging.Level;
import com.gs.utils.MessageBox;
import java.awt.CardLayout;
import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.io.File;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.Date;
import java.util.Properties;
import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.activation.FileDataSource;
import javax.mail.Authenticator;
import javax.mail.BodyPart;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import javax.swing.ActionMap;
import javax.swing.JPanel;

/**
 *
 * @author Sean Carrick
 */
public class WizardDialog extends javax.swing.JDialog 
        implements PropertyChangeListener{

    private final NTApp app;
    private final ActionMap actions;
    private final ResourceMap resources;
    private final LogRecord record;
    private final Image bgImage;
    private final Font stdFont;
    private final Color stdColor;
    private final String[] steps = new String[]{
        "Welcome",
        "Error Information",
        "Application/System Information",
        "Steps to Reproduce"
    };
    private int onStep;
    private int prevStep;

    /**
     * Creates new form WizardDialog
     */
    public WizardDialog(final NTApp app, final LogRecord record,
            java.awt.Frame parent, boolean modal) {
        super(parent, modal);
        this.app = app;
        actions = app.getContext().getActionMap(this);
        resources = app.getContext().getResourceMap(getClass());
        this.record = record;
        bgImage = resources.getImageIcon("bgImage").getImage();
        
        onStep = 0;
        prevStep = -1;
        
        initComponents();
        
        stdFont = welcomePage.getFont();
        stdColor = welcomePage.getForeground();
    }
    
    public boolean isAbleToGoOn() {
        return onStep < steps.length - 1;
    }
    
    public void setAbleToGoOn(boolean canContinue) {
        // ignored.
    }
    
    public boolean isAbleToBackup() {
        return prevStep >= 0;
    }
    
    public void setAbleToBackup(boolean canBackup) {
        // ignored.
    }
    
    public boolean isAbleToFinish() {
        return onStep == steps.length - 1;
    }
    
    public void setAbleToFinish(boolean canFinish) {
        // ignored.
    }
    
    @Action
    public void showHelp() {
        
    }
    
    @Action
    public void cancel() {
        int choice = MessageBox.askQuestion("Are you sure you want to cancel?", 
                "Confirm Cancel", false);
        if (choice == MessageBox.YES_OPTION) {
            dispose();
        }
    }
    
    @Action(enabledProperty = "ableToGoOn")
    public void goForward() {
        prevStep = onStep++;
        ((CardLayout) contentArea.getLayout()).show(contentArea, "step" + onStep);
        
        app.getPropertyChangeSupport().firePropertyChange("isAbleToGoOn", isAbleToBackup(), isAbleToGoOn());
        app.getPropertyChangeSupport().firePropertyChange("isAbleToGoBack", isAbleToGoOn(), isAbleToBackup());
        app.getPropertyChangeSupport().firePropertyChange("isAbleToFinish", isAbleToGoOn(), isAbleToFinish());
        update(getGraphics());
        String msg = String.format("steps = %s\n"
                + "prevStep = %s\n"
                + "onStep = %s\n"
                + "isAbleToBackup() = %s\n"
                + "isAbleToGoOn() = %s\n"
                + "isAbleToFinish() = %s", steps.length, prevStep, onStep, isAbleToBackup(),
                isAbleToGoOn(), isAbleToFinish());
        app.getLogger().log(Level.TRACE, msg);
        
    }
    
    @Action(enabledProperty = "ableToBackup")
    public void goBack() {
        prevStep--;
        onStep--;
        
        ((CardLayout) contentArea.getLayout()).show(contentArea, "step" + onStep);
        
        app.getPropertyChangeSupport().firePropertyChange("isAbleToGoOn", prevStep, onStep);
        app.getPropertyChangeSupport().firePropertyChange("isAbleToGoBack", prevStep - 1, prevStep);
        app.getPropertyChangeSupport().firePropertyChange("isAbleToFinish", prevStep, onStep);
        update(getGraphics());
        String msg = String.format("steps = %s\n"
                + "prevStep = %s\n"
                + "onStep = %s\n"
                + "isAbleToBackup() = %s\n"
                + "isAbleToGoOn() = %s\n"
                + "isAbleToFinish() = %s", steps.length, prevStep, onStep, isAbleToBackup(),
                isAbleToGoOn(), isAbleToFinish());
        app.getLogger().log(Level.TRACE, msg);
        
    }
    
    @Action(enabledProperty = "ableToFinish")
    public Task submit() {
        SendBugReportTask submitTask = new SendBugReportTask(app);
        
        setVisible(false);
        
        return submitTask;
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        basePanel = new javax.swing.JPanel();
        jSeparator2 = new javax.swing.JSeparator();
        commandPanel = new javax.swing.JPanel();
        cancelButton = new javax.swing.JButton();
        submitButton = new javax.swing.JButton();
        nextButton = new javax.swing.JButton();
        backButton = new javax.swing.JButton();
        helpButton = new javax.swing.JButton();
        sideBar = new SidebarPanel();
        contentArea = new javax.swing.JPanel();
        welcomePage = new javax.swing.JPanel();
        welcomeLabel = new javax.swing.JLabel();
        errorPage = new javax.swing.JPanel();
        jScrollPane2 = new javax.swing.JScrollPane();
        errorInfoPane = new javax.swing.JEditorPane();
        appSysInfoPage = new javax.swing.JPanel();
        jScrollPane1 = new javax.swing.JScrollPane();
        appSysInfoPane = new javax.swing.JEditorPane();
        stepsPage = new javax.swing.JPanel();
        instructionsLabel = new javax.swing.JLabel();
        jScrollPane3 = new javax.swing.JScrollPane();
        stepsField = new javax.swing.JTextArea();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setName("Form"); // NOI18N

        basePanel.setName("basePanel"); // NOI18N
        basePanel.setLayout(new java.awt.BorderLayout());

        jSeparator2.setName("jSeparator2"); // NOI18N
        basePanel.add(jSeparator2, java.awt.BorderLayout.PAGE_START);

        commandPanel.setMaximumSize(new java.awt.Dimension(32767, 50));
        commandPanel.setMinimumSize(new java.awt.Dimension(100, 50));
        commandPanel.setName("commandPanel"); // NOI18N

        cancelButton.setText("Cancel");
        cancelButton.setName("cancelButton"); // NOI18N
        cancelButton.setAction(actions.get("cancel"));

        submitButton.setText("Submit");
        submitButton.setName("submitButton"); // NOI18N
        submitButton.setAction(actions.get("submit"));

        nextButton.setText("Next");
        nextButton.setHorizontalTextPosition(javax.swing.SwingConstants.LEADING);
        nextButton.setName("nextButton"); // NOI18N
        nextButton.setAction(actions.get("goForward"));

        backButton.setText("Back");
        backButton.setName("backButton"); // NOI18N
        backButton.setAction(actions.get("goBack"));

        helpButton.setText("Help");
        helpButton.setName("helpButton"); // NOI18N
        helpButton.setAction(actions.get("showHelp"));

        javax.swing.GroupLayout commandPanelLayout = new javax.swing.GroupLayout(commandPanel);
        commandPanel.setLayout(commandPanelLayout);
        commandPanelLayout.setHorizontalGroup(
            commandPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, commandPanelLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(helpButton)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 563, Short.MAX_VALUE)
                .addComponent(backButton)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(nextButton)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(submitButton)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(cancelButton)
                .addContainerGap())
        );
        commandPanelLayout.setVerticalGroup(
            commandPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(commandPanelLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(commandPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(cancelButton)
                    .addComponent(submitButton)
                    .addComponent(nextButton)
                    .addComponent(backButton)
                    .addComponent(helpButton))
                .addContainerGap(15, Short.MAX_VALUE))
        );

        basePanel.add(commandPanel, java.awt.BorderLayout.CENTER);

        getContentPane().add(basePanel, java.awt.BorderLayout.SOUTH);

        sideBar.setMaximumSize(new java.awt.Dimension(200, 32767));
        sideBar.setMinimumSize(new java.awt.Dimension(200, 300));
        sideBar.setName("sideBar"); // NOI18N

        javax.swing.GroupLayout sideBarLayout = new javax.swing.GroupLayout(sideBar);
        sideBar.setLayout(sideBarLayout);
        sideBarLayout.setHorizontalGroup(
            sideBarLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 200, Short.MAX_VALUE)
        );
        sideBarLayout.setVerticalGroup(
            sideBarLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 535, Short.MAX_VALUE)
        );

        getContentPane().add(sideBar, java.awt.BorderLayout.LINE_START);

        contentArea.setName("contentArea"); // NOI18N
        contentArea.setLayout(new java.awt.CardLayout());

        welcomePage.setName("welcomePage"); // NOI18N

        welcomeLabel.setText("welcomeLabel");
        welcomeLabel.setText(resources.getString("welcomeLabel.text"));
        welcomeLabel.setVerticalAlignment(javax.swing.SwingConstants.TOP);
        welcomeLabel.setName("welcomeLabel"); // NOI18N

        javax.swing.GroupLayout welcomePageLayout = new javax.swing.GroupLayout(welcomePage);
        welcomePage.setLayout(welcomePageLayout);
        welcomePageLayout.setHorizontalGroup(
            welcomePageLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, welcomePageLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(welcomeLabel, javax.swing.GroupLayout.DEFAULT_SIZE, 702, Short.MAX_VALUE)
                .addContainerGap())
        );
        welcomePageLayout.setVerticalGroup(
            welcomePageLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, welcomePageLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(welcomeLabel, javax.swing.GroupLayout.DEFAULT_SIZE, 511, Short.MAX_VALUE)
                .addContainerGap())
        );

        contentArea.add(welcomePage, "step0");

        errorPage.setName("errorPage"); // NOI18N

        jScrollPane2.setName("jScrollPane2"); // NOI18N

        errorInfoPane.setContentType("text/html"); // NOI18N
        errorInfoPane.setText("<html>\n  <head>\n\n  </head>\n  <body>\n     <h1><hr> <em>I am italicized</em><hr></h1>\n  </body>\n</html>\n");
        errorInfoPane.setName("errorInfoPane"); // NOI18N
        jScrollPane2.setViewportView(errorInfoPane);

        javax.swing.GroupLayout errorPageLayout = new javax.swing.GroupLayout(errorPage);
        errorPage.setLayout(errorPageLayout);
        errorPageLayout.setHorizontalGroup(
            errorPageLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, errorPageLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane2, javax.swing.GroupLayout.DEFAULT_SIZE, 702, Short.MAX_VALUE)
                .addContainerGap())
        );
        errorPageLayout.setVerticalGroup(
            errorPageLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, errorPageLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane2, javax.swing.GroupLayout.DEFAULT_SIZE, 511, Short.MAX_VALUE)
                .addContainerGap())
        );

        contentArea.add(errorPage, "step1");

        appSysInfoPage.setName("appSysInfoPage"); // NOI18N

        jScrollPane1.setName("jScrollPane1"); // NOI18N

        appSysInfoPane.setContentType("text/html"); // NOI18N
        appSysInfoPane.setText("<html>\n  <head>\n\n  </head>\n  <body>\n    <h1><hr><em>Application Information</em><hr></h1>\n    <p margin=\"0\">\n    \n    </p>\n    <h1><hr><em>Java Runtime and VM Information</em><hr></h1>\n    <p margin=\"0\">\n    \n    </p>\n    <h1><hr><em>Operating System Information</em><hr></h1>\n    <p margin=\"0\">\n    \n    </p>\n  </body>\n</html>\n");
        appSysInfoPane.setName("appSysInfoPane"); // NOI18N
        jScrollPane1.setViewportView(appSysInfoPane);

        javax.swing.GroupLayout appSysInfoPageLayout = new javax.swing.GroupLayout(appSysInfoPage);
        appSysInfoPage.setLayout(appSysInfoPageLayout);
        appSysInfoPageLayout.setHorizontalGroup(
            appSysInfoPageLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(appSysInfoPageLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 702, Short.MAX_VALUE)
                .addContainerGap())
        );
        appSysInfoPageLayout.setVerticalGroup(
            appSysInfoPageLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(appSysInfoPageLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 511, Short.MAX_VALUE)
                .addContainerGap())
        );

        contentArea.add(appSysInfoPage, "step2");

        stepsPage.setName("stepsPage"); // NOI18N

        instructionsLabel.setText("<html><h4>Steps to Reproduce the Error</h4><p>On this page, you can enter steps that we can take to reproduce the error that generated this <em>Bug Report Wizard</em>. In thes steps, you can use standard markdown such as this:\n<ul><li><em>italics</em> are rendered by using <code>*italics*</code></li>\n<li><strong>bold</strong> is rendered by using <code>**bold**</code></li>\n<li><strong><em>bold, italics</em></strong> is rendered by using <code>***bold, italics***</code></li>\n<li>Bulleted lists like this are rendered by using <code>* </code> (asterisk space)</li>\n<li>Numbered lists are rendered by using <code>1. </code>, <code>2. </code>, etc. You can even use the same number for each item.</li>\n<li><code>Code/Monospace text</code> is rendered using <code>`Code/Monospaced text`</code></li></ul>\nThe rendering will not be visible to you in this editor, but will cause formatting of the text on the Issues Server.");
        instructionsLabel.setText(resources.getString("instructionsLabel.text"));
        instructionsLabel.setVerticalAlignment(javax.swing.SwingConstants.TOP);
        instructionsLabel.setName("instructionsLabel"); // NOI18N

        jScrollPane3.setName("jScrollPane3"); // NOI18N

        stepsField.setColumns(20);
        stepsField.setRows(5);
        stepsField.setName("stepsField"); // NOI18N
        jScrollPane3.setViewportView(stepsField);

        javax.swing.GroupLayout stepsPageLayout = new javax.swing.GroupLayout(stepsPage);
        stepsPage.setLayout(stepsPageLayout);
        stepsPageLayout.setHorizontalGroup(
            stepsPageLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(stepsPageLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(stepsPageLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(instructionsLabel, javax.swing.GroupLayout.DEFAULT_SIZE, 702, Short.MAX_VALUE)
                    .addComponent(jScrollPane3))
                .addContainerGap())
        );
        stepsPageLayout.setVerticalGroup(
            stepsPageLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(stepsPageLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(instructionsLabel, javax.swing.GroupLayout.PREFERRED_SIZE, 226, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane3, javax.swing.GroupLayout.PREFERRED_SIZE, 279, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );

        contentArea.add(stepsPage, "step3");

        getContentPane().add(contentArea, java.awt.BorderLayout.CENTER);
        app.addPropertyChangeListener("isAbleToGoBack", this);
        app.addPropertyChangeListener("isAbleToGoOn", this);
        app.addPropertyChangeListener("isAbleToFinish", this);

        setTitle(resources.getString("Form.title"));
        setIconImage(resources.getImageIcon("Form.iconImage").getImage());
        addWindowListener(new BugReportListener());

        backButton.setEnabled(isAbleToBackup());
        nextButton.setEnabled(isAbleToGoOn());
        nextButton.setVisible(!isAbleToFinish());
        submitButton.setVisible(isAbleToFinish());

        // Build the error information page:
        StringBuilder sb = new StringBuilder();
        sb.append("<html><h1><hr><em>Gathered Error Information</em><hr></h1>");
        sb.append("<h3>Exception Thrown: (Thrown at ").append(record.getInstant().toString()).append(")</h3>");
        sb.append("<p><strong><em>Exception:</em></strong> ").append(record.getThrown().getClass().toString());
        sb.append("<br><strong><em>Thrown At:</em></strong>").append(record.getSourceClassName());
        sb.append(".").append(record.getSourceMethodName()).append(" [Sequence #");
        sb.append(record.getSequenceNumber()).append("]");
        sb.append("<br><strong><em>Message:</em></strong> ").append(record.getThrown().getMessage());
        if (record.getThrown().getCause() != null) {
            sb.append("<br><strong><em>Cause:</em></strong> ").append(record.getThrown().getCause().toString());
        }
        sb.append("<br><strong><em>Full Stack Trace:<br>");

        if (record.getThrown().getStackTrace() != null && record.getThrown().getStackTrace().length > 0) {
            for (StackTraceElement e : record.getThrown().getStackTrace()) {
                sb.append("<br>" + e.toString());
            }
        } else {
            sb.append("<br>&ndash; No Stack Trace Available &ndash;<br>");
        }
        sb.append("<table border='1'><caption>Thread Information</caption>");
        sb.append("<th><td>Name</td><td>ID</td><td>Priority</td><td>State</td><td>Is Alive</td><td>Is Daemon</td></th>");
        sb.append("<tr><td>").append(record.getThread().getName()).append("</td>");
        sb.append("<td>").append(record.getThread().getId()).append("</td>");
        sb.append("<td>").append(record.getThread().getPriority()).append("</td>");
        sb.append("<td>").append(record.getThread().getState().toString()).append("</td>");
        sb.append("<td>").append(record.getThread().isAlive()).append("</td>");
        sb.append("<td>").append(record.getThread().isDaemon()).append("</td>");
        sb.append("</tr></table>");

        errorInfoPane.setText(sb.toString());
        errorInfoPane.setCaretPosition(0);

        sb = new StringBuilder();

        sb.append("<html><h1><hr><em>Application Information</em><hr></h1>");
        sb.append("<p><strong><em>Application Title:</em></strong> ");
        sb.append(resources.getString("Application.title")).append("<br>");
        sb.append("<strong><em>Application Version:</em></strong> ");
        sb.append(resources.getString("Application.version"));
        sb.append(" Build ").append(resources.getString("Application.build"));
        sb.append("<br><strong><em>Vendor Name:</em></strong> ");
        sb.append(resources.getString("Application.vendor")).append("<br>");
        sb.append("<strong><em>Vendor Website:</em></strong> ");
        sb.append(resources.getString("Application.homepage")).append("<br>");
        sb.append("</p><h4>Installed Modules:</h4>");

        for (Module m : app.getInstalledModules()) {
            sb.append(m.getPluginName()).append(" (").append(m.getVendorName());
            sb.append(") v. ").append(m.getVersion()).append("<br>");
        }

        sb.append("<h1><hr><em>Operating System Information</em><hr></h1>");
        sb.append("<p><strong><em>Operating System:</em></strong> ");
        sb.append(System.getProperty("os.name")).append(" (v. ");
        sb.append(System.getProperty("os.version")).append(")<br>");
        sb.append("<br><strong><em>OS Vendor:");
        sb.append("</em></strong> ");
        String osname = System.getProperty("os.name");
        if (osname.toLowerCase().contains("windows")) {
            sb.append("Microsoft Corporation").append("<br>");
        } else if (osname.toLowerCase().contains("mac os")) {
            sb.append("Apple Computers").append("<br>");
        } else if (osname.toLowerCase().contains("solaris")) {
            sb.append("Oracle, Inc. (by way of Sun Microsystems, Inc.)<br>");
        } else {
            sb.append("Brought to you by Linus Torvalds, et. al.<br>");
        }
        sb.append("<strong><em>OS Architecture:</strong><em> ");
        sb.append(System.getProperty("os.arch")).append("</p><br>");

        sb.append("<h1><hr><em>Java System Information</em><hr></h1>");
        sb.append("<p><strong><em>Java Runtime:</em></strong>");
        sb.append(System.getProperty("java.runtime.name")).append(" (v. ");
        sb.append(System.getProperty("java.runtime.version")).append(")<br>");
        sb.append("<strong><em>Specification:</em></strong>");
        sb.append(System.getProperty("java.specification.name")).append("(v. ");
        sb.append(System.getProperty("java.specification.version")).append(") by ");
        sb.append(System.getProperty("java.specification.vendor")).append("<br>");
        sb.append("<strong><em>Vendor:</em></strong> ");
        sb.append(System.getProperty("java.vendor")).append(" (v. ");
        sb.append(System.getProperty("java.vendor.version")).append(")<br>");
        sb.append("&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<strong><em>Website:</em></strong> ");
        sb.append(System.getProperty("java.vendor.url")).append("<br>&nbsp;&nbsp;&nbsp;");
        sb.append("&nbsp;&nbsp;<strong><em>Bugs Website:</em></strong> ");
        sb.append(System.getProperty("java.vendor.url.bug")).append("<br><br><strong>");
        sb.append("<em>Class Path:</em></strong><br>");
        sb.append(System.getProperty("java.class.path"));

        appSysInfoPane.setText(sb.toString());
        appSysInfoPane.setCaretPosition(0);

        pack();
    }// </editor-fold>//GEN-END:initComponents

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JPanel appSysInfoPage;
    private javax.swing.JEditorPane appSysInfoPane;
    private javax.swing.JButton backButton;
    private javax.swing.JPanel basePanel;
    private javax.swing.JButton cancelButton;
    private javax.swing.JPanel commandPanel;
    private javax.swing.JPanel contentArea;
    private javax.swing.JEditorPane errorInfoPane;
    private javax.swing.JPanel errorPage;
    private javax.swing.JButton helpButton;
    private javax.swing.JLabel instructionsLabel;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JScrollPane jScrollPane3;
    private javax.swing.JSeparator jSeparator2;
    private javax.swing.JButton nextButton;
    private javax.swing.JPanel sideBar;
    private javax.swing.JTextArea stepsField;
    private javax.swing.JPanel stepsPage;
    private javax.swing.JButton submitButton;
    private javax.swing.JLabel welcomeLabel;
    private javax.swing.JPanel welcomePage;
    // End of variables declaration//GEN-END:variables

    
    private class SidebarPanel extends JPanel {

        @Override
        public void paintComponent(Graphics g) {
            super.paintComponent(g);
            java.awt.Graphics2D g2d = (java.awt.Graphics2D) g;

            g2d.drawImage(bgImage, 0, 0, this);

            for (int x = 0; x < steps.length; x++) {
                if (x == onStep) {
                    g2d.setColor(Color.blue);
                    g2d.setFont(new Font("Dialog", Font.BOLD, 13));
                } else {
                    g2d.setFont(stdFont);
                    g2d.setColor(stdColor);
                }

                g2d.drawString(steps[x], 5, 15 * (x + 1));
            }
        }
    }

    @Override
    public void propertyChange(PropertyChangeEvent pce) {
            nextButton.setEnabled(isAbleToGoOn());
            nextButton.setVisible(isAbleToGoOn());
            backButton.setEnabled(isAbleToBackup());
            submitButton.setVisible(isAbleToFinish());
            submitButton.setEnabled(isAbleToFinish());
    }
    
    private class BugReportListener extends WindowAdapter {

        @Override
        public void windowClosing(WindowEvent e) {
            String fileName = WizardDialog.this.getClass().getName()
                    + WizardDialog.this.getTitle().replace(' ', '-')
                    + ".xml";
            
            try {
                app.getContext().getSessionStorage().save(WizardDialog.this, 
                        fileName);
            } catch (IOException ex) {
                String msg = String.format("Unable to save state for %s at %s", 
                        WizardDialog.this, fileName);
                app.getLogger().log(Level.WARN, msg);
            }
        }

        @Override
        public void windowOpened(WindowEvent e) {
            String fileName = WizardDialog.this.getClass().getName()
                    + WizardDialog.this.getTitle().replace(' ', '-')
                    + ".xml";
            
            try {
                app.getContext().getSessionStorage().restore(WizardDialog.this, 
                        fileName);
            } catch (IOException ex) {
                String msg = String.format("Unable to restore previous state "
                        + "for %s from %s", WizardDialog.this, fileName);
            }
        }
        
    }
    
    private class SendBugReportTask extends Task<Boolean, Void> {
        
        private final NTApp app;
        
        public SendBugReportTask(Application application) {
            super(application);
            app = (NTApp) application;
        }
        
        @Override
        protected Boolean doInBackground() throws Exception {
            setProgress(0, 0, 100);
            setMessage("Preparing bug report...");
            
            final String fromEmail = "nta-bugs@gs-unitedlabs.com";
            final char[] pword = new char[] { '5', '9', '2', '*', '-', ':', '1',
                '8', 'u', '6', 'R', '3', 'p', '0', 'r', 't', '3', 'r' };
            final String toEmail = "contact-project+gs-unitedlabs-northwind-"
                    + "traders-32830006-issue-@incoming.gitlab.com";
            String msg = String.format("Sending bug report to %s, from %s", 
                    toEmail, fromEmail);
            app.getLogger().log(Level.DEBUG, msg);
            
            Properties props = new Properties();
            props.put("mail.smtp.host", "smtp.gmail.com");
            props.put("mail.smtp.socketFactory.port", "465");
            props.put("mail.smtp.socketFactory.class", "javax.net.ssl.SSLSocketFactory");
            props.put("mail.smtp.auth", "true");
            msg = String.format("Using these properties for creating the "
                    + "Session: %s", props);
            app.getLogger().log(Level.CONFIG, msg);
            
            setProgress(0.1f);
            app.getLogger().log(Level.TRACE, "Creating the Authenticator for "
                    + "SSL authentication.");
            Authenticator auth = new Authenticator() {
                @Override
                protected PasswordAuthentication getPasswordAuthentication() {
                    return new PasswordAuthentication(fromEmail, 
                            String.copyValueOf(pword));
                }
            };
            
            setProgress(0.15f);
            setMessage("Creating email session...");
            Session session = Session.getDefaultInstance(props, auth);
            
            try {
                setProgress(0.25f);
                setMessage("Building report message...");
                MimeMessage message = new MimeMessage(session);

                // Set message headers:
                message.addHeader("Content-type", "text/HTML; charset=UTF-8");
                message.addHeader("format", "flowed");
                message.addHeader("Content-Transfer-Encoding", "8bit");
                message.setFrom(new InternetAddress("nta-bugs@gs-unitedlabs.com",
                        "Bug Reporter"));
                String replyTo = app.getProperty("company.email").toString();
                if (replyTo == null || replyTo.isBlank() || replyTo.isEmpty()) {
                    replyTo = "no-reply@nowhere.com";
                }
                message.setReplyTo(InternetAddress.parse(replyTo, false));
                message.setSubject("[BUG] " + record.getThrown().getClass().getName()
                        + " in " + record.getSourceClassName() 
                        + "." + record.getSourceMethodName(), "UTF-8");
                message.setSentDate(new Date());
                message.setRecipients(Message.RecipientType.TO, 
                        InternetAddress.parse(toEmail, false));
                
                app.getLogger().log(Level.TRACE, "Creating a MimeBodyPart "
                        + "instance to use for the various parts of the email "
                        + "body.");
                BodyPart messageBodyPart = new MimeBodyPart();
                messageBodyPart.setText(getBody());
                
                app.getLogger().log(Level.TRACE, "Creating a MimeMultipart "
                        + "instance for attaching the various parts of the "
                        + "email.");
                Multipart multipart = new MimeMultipart();
                
                app.getLogger().log(Level.TRACE, "Attaching the body of the "
                        + "email to the MimeMultipart instance.");
                multipart.addBodyPart(messageBodyPart);
                
                app.getLogger().log(Level.TRACE, "Resetting the messageBodyPart "
                        + "to add an attachment.");
                messageBodyPart = new MimeBodyPart();
                
                app.getLogger().log(Level.TRACE, "Getting a File object to the "
                        + "application log file.");
                File file = new File(app.getLogDirectory(), "NTA-Basic.log");
                setProgress(0.5f);
                setMessage("Attaching application log file...");
                DataSource source = new FileDataSource(file);
                messageBodyPart.setDataHandler(new DataHandler(source));
                messageBodyPart.setFileName(file.getAbsolutePath());
                multipart.addBodyPart(messageBodyPart);
                
                message.setContent(multipart);
                
                setProgress(0.75f);
                setMessage("Opening transport to send bug report...");
                Transport.send(message);
                
                setProgress(1.0f);
                setMessage("Bug report successfully sent.");
            } catch (UnsupportedEncodingException | MessagingException ex) {
                String message;
                if (ex instanceof UnsupportedEncodingException) {
                    message = String.format("Unsupported encoding: %s", ex);
                } else if (ex instanceof MessagingException) {
                    message = String.format("Messaging exception: %s", ex);
                } else {
                    message = String.format("Unknown error: %s", ex);
                }
                
                app.getLogger().log(Level.WARN, message);
                return false;
            }
            
            return true;
        }
        
        private String getBody() {
            StringBuilder sb = new StringBuilder();
            sb.append(stepsField.getText()).append("\n\n----\n\n");
            sb.append("***System Information***:\n\n");
            sb.append("* ***OS Name***: ").append(System.getProperty("os.name"));
            sb.append("\n* ***Version***: ").append(System.getProperty("os.version"));
            sb.append("\n* ***Architecture***: ").append(System.getProperty("os.arch"));
            sb.append("\n\n----\n\n***User Information***\n\n");
            sb.append("* ***User Home***: ").append(System.getProperty("user.home"));
            sb.append("\n* ***User Directory***: ").append(System.getProperty("user.dir"));
            sb.append("\n* ***Country***: ").append(System.getProperty("user.country"));
            sb.append("\n* ***Language***: ").append(System.getProperty("user.language"));
            sb.append("\n* ***Time Zone***: ").append(System.getProperty("user.timezone"));
            sb.append("\n\n----\n\n***Application Information***\n\n");
            sb.append("* ***Application Title***: ").append(resources.getString("Application.title"));
            sb.append("\n* ***Version & Build***: ").append(resources.getString("Application.version"));
            
            return sb.toString();
        }
        
    }

}
