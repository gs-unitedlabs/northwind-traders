/*
 * Copyright (C) 2022 GS United Labs
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * *****************************************************************************
 *  Project    :   NTA-Basic
 *  Class      :   BugReportingModule.java
 *  Author     :   Sean Carrick
 *  Created    :   Jan 19, 2022 @ 12:24:18 PM
 *  Modified   :   Jan 19, 2022
 *  
 *  Purpose: See class JavaDoc
 *  
 *  Revision History:
 *  
 *   WHEN          BY                  REASON
 *   ------------  ------------------- ------------------------------------------
 *  Jan 19, 2022  Sean Carrick        Initial creation.
 * *****************************************************************************
 */

package com.gs.nta.bugs;

import com.gs.application.Application;
import com.gs.spi.desktop.TaskPaneProvider;
import com.gs.spi.modules.Module;
import java.util.ArrayList;
import java.util.List;
import javax.swing.ImageIcon;

/**
 *
 * @author Sean Carrick &lt;sean at gs-unitedlabs dot com&gt;
 * 
 * @version 1.0
 * @since 1.0
 */
public class BugReportingModule implements Module {
    
    private Application app = Application.getInstance();
    
    public BugReportingModule () {
        
    }

    @Override
    public String getVersion() {
        return app
                .getContext()
                .getResourceMap(BugReportingModule.class)
                .getString("module.version");
    }

    @Override
    public TaskPaneProvider getTaskPane() {
        //TODO: Add implementation in com.gs.nta.bugs.BugReportingModule.getTaskPane.
        return Module.super.getTaskPane();
    }

    @Override
    public String getPluginName() {
        return app
                .getContext()
                .getResourceMap(BugReportingModule.class)
                .getString("module.title");
    }

    @Override
    public String getVendorName() {
        return app
                .getContext()
                .getResourceMap(BugReportingModule.class)
                .getString("module.vendor");
    }

    @Override
    public String getCopyright() {
        return app
                .getContext()
                .getResourceMap(BugReportingModule.class)
                .getString("module.copyright");
    }

    @Override
    public List<String> getContributors() {
        List<String> list = new ArrayList<>();
        list.add("Sean Carrick: Founding Partner, Lead Developer; Illinois, USA");
        return list;
    }

    @Override
    public String getDescription() {
        return app
                .getContext()
                .getResourceMap(BugReportingModule.class)
                .getString("module.description");
    }

    @Override
    public ImageIcon getImageIcon() {
        return app
                .getContext()
                .getResourceMap(BugReportingModule.class)
                .getImageIcon("module.icon");
    }

    @Override
    public int getAboutTabPosition() {
        return 59000;
    }

}
