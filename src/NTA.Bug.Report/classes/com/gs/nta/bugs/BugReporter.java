/*
 * Copyright (C) 2022 GS United Labs
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * *****************************************************************************
 *  Project    :   NTA-Basic
 *  Class      :   BugReporter.java
 *  Author     :   Sean Carrick
 *  Created    :   Jan 20, 2022 @ 8:56:34 AM
 *  Modified   :   Jan 20, 2022
 *  
 *  Purpose: See class JavaDoc
 *  
 *  Revision History:
 *  
 *   WHEN          BY                  REASON
 *   ------------  ------------------- ------------------------------------------
 *  Jan 20, 2022  Sean Carrick        Initial creation.
 * *****************************************************************************
 */
package com.gs.nta.bugs;

import com.gs.api.logging.LogRecord;
import com.gs.application.Application;
import com.gs.application.ResourceMap;
import com.gs.nta.NTApp;
import com.gs.nta.bugs.wizard.WizardDialog;
import java.awt.Toolkit;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 *
 * @author Sean Carrick &lt;sean at gs-unitedlabs dot com&gt;
 *
 * @version 1.0
 * @since 1.0
 */
public class BugReporter implements com.gs.spi.bugs.BugReporter {

    private final NTApp app;
    private final Map<String, Object> values;
    
    private String errorFileName;
    private String logFileName;
    private LogRecord record;

    public BugReporter() {
        app = (NTApp) Application.getInstance(NTApp.class);
        values = new HashMap();
    }
    
    public BugReporter(final NTApp app, LogRecord errorRecord) {
        this.app = app;
        this.record = errorRecord;
        values = new HashMap<>();

        loadValuesFromRecord();
        loadValuesFromSystemProperties();
        loadValuesFromApplicationProperties();
    }

    private void loadValuesFromRecord() {
        values.put("errorField", record.getThrown().getClass().getName());
        values.put("messageField", record.getThrown().getMessage());
        values.put("threadField", record.getThread().getName());
        values.put("threadIdField", record.getThread().getId());
        values.put("stateField", record.getThread().getState());
        values.put("priorityField", record.getThread().getPriority());
        values.put("aliveCheckbox", record.getThread().isAlive());
        values.put("daemonCheckbox", record.getThread().isDaemon());

        if (record.getThrown().getSuppressed().length > 0) {
            Throwable[] thrown = new Throwable[record.getThrown().getSuppressed().length];
            System.arraycopy(record.getThrown().getSuppressed(), 0, thrown, 0, thrown.length);
            values.put("causeList", thrown);
        }
        
        if (record.getThrown().getStackTrace().length > 0) {
            StackTraceElement[] elements = new StackTraceElement[record.getThrown().getStackTrace().length];
            System.arraycopy(record.getThrown().getStackTrace(), 0, elements, 0, elements.length);
            values.put("stackTraceField", elements);
        }
    }

    private void loadValuesFromSystemProperties() {
        ResourceMap m = app.getContext().getResourceMap();

        values.put("appTitleField", m.getString("Application.title"));
        values.put("appVersionField", m.getString("Application.version"));
        values.put("appBuildField", m.getString("Application.build"));
        values.put("appVendorField", m.getString("Application.vendor"));

        if (!app.getInstalledModules().isEmpty()) {
            String[] modules = new String[app.getInstalledModules().size()];
            List<com.gs.spi.modules.Module> mods = app.getInstalledModules();
            for (int x = 0; x < modules.length; x++) {
                modules[x] = mods.get(x).getPluginName() + " ["
                        + mods.get(x).getVendorName() + "]: v. "
                        + mods.get(x).getVersion();
            }
        }
    }

    private void loadValuesFromApplicationProperties() {
        values.put("javaTitleField", System.getProperty("java.runtime.name"));
        values.put("javaVersionField", System.getProperty("java.runtime.version"));
        values.put("javaVendorField", System.getProperty("java.vendor"));
        values.put("hotspotField", System.getProperty("java.vm.name"));

        values.put("osNameField", System.getProperty("os.name"));
        values.put("osVersionField", System.getProperty("os.version"));
        values.put("archField", System.getProperty("os.arch"));

        if (values.get("osNameField").toString().toLowerCase().contains("windows")) {
            values.put("osVendorField", "Microsoft");
        } else if (values.get("osNameField").toString().toLowerCase().contains("mac os")) {
            values.put("osVendorField", "Apple Computers, Inc.");
        } else if (values.get("osNameField").toString().toLowerCase().contains("solaris")) {
            values.put("osVendorField", "Oracle, Inc. (by way of Sun Microsystems");
        } else {
            values.put("osVendorField", "Linux courtesy of Linux Torvalds, et. al.");
        }
    }

    @Override
    public String getErrFileName() {
        return errorFileName;
    }

    @Override
    public void setErrFileName(String errorLogFileName) {
        this.errorFileName = errorLogFileName;
    }

    @Override
    public String getLogFileName() {
        return logFileName;
    }

    @Override
    public void setLogFileName(String logFileName) {
        this.logFileName = logFileName;
    }

    @Override
    public com.gs.spi.logging.LogRecord getLogRecord() {
        return record;
    }

    @Override
    public void setLogRecord(com.gs.spi.logging.LogRecord record) {
        this.record = (com.gs.api.logging.LogRecord) record;
    }

    @Override
    public void showReport() {
        int width = 1000;
        int height = 860;
        int x = (Toolkit.getDefaultToolkit().getScreenSize().width - width) / 2;
        int y = (Toolkit.getDefaultToolkit().getScreenSize().height - height) / 2;
        
        WizardDialog p = new WizardDialog(app, record, app.getMainFrame(), true);
        p.pack();
        p.setVisible(true);
    }

}
